%%% Fichier mis à disposition selon la licence CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) %%%
\documentclass[A4,12pt]{cueep}
\renewcommand{\SFC}{\includegraphics[height=8mm]{../logo_lille1}\includegraphics[height=8mm]{../ccbyncsa}}
 
% rubber: setlist arguments

%%% Local Variables:
%%% mode: pdflatex
%%% coding: utf-8
%%% LaTeX-command: "pdflatex"
%%% End: 





\titre{Résolution d'un système linéaire}


\usepackage{draftwatermark}
\SetWatermarkScale{1.7}
\SetWatermarkAngle{300}
\SetWatermarkLightness{0.95}
\SetWatermarkText{Math 9}
\usepackage[tikz]{bclogo}

\ladate{0.99}
\reference{Memo}

\motsclefs{systèmes, pivot, DAEU, Math 9}

\auteur{Sébastien Dumoulard}
\organisme{Service de Formation Continue}

\begin{document}

Voici une méthode de résolution de systèmes linéaires, appelée pivot de Gauss. Notons qu'il s'agit d'une méthode algorithmique, donc facilement programmable.\\
Considérons les deux systèmes suivants :\\
$(S_1) : \syst{2x+y-z=1}{x+y+z=6 }{-x-y+z=0}$ et $(S_2) : \syst{x+y+z=6}{y+3z=11 }{z=3}$\\
Le second système peut être qualifié de ``\textbf{facile}'', puisqu'une \textbf{substitution à rebours} permet de le résoudre très vite :\\
la dernière équation donne immédiatement $z=3$, que l'on remplace alors dans la deuxième pour obtenir\\$y=11-3\times 3 = 2$, que l'on remplace enfin dans la première pour obtenir $x=6-2-3=1$.\\
\textbf{Ainsi la solution du second système $(S_2)$ est le triplet $(1~;~2~;~3)$.\\}
Nous allons démontrer que le premier système $(S_1)$, d'apparence moins simple, est en fait équivalent au système $(S_2)$ :\\
Il est clair qu'intervertir deux équations ne change pas les solutions d'un système. Donc $(S_1)$ équivaut à $\syst{x+y+z=6 }{2x+y-z=1}{-x-y+z=0}$.\\
De même, si on ajoute la première ligne à la troisième, on ajoute des quantités égales (ici $x+y+z$ et $6$) à chaque membre de la troisième équation, qui ne change donc pas.\\
$(S_1)$ équivaut donc à $\syst{x+y+z=6 }{2x+y-z=1}{2z=6}$.\\
Avec le même argument, on peut ajouter $-2$ fois la première ligne à la deuxième, et donc\\
$(S_1)$ équivaut à $\syst{x+y+z=6 }{-y-3z=-11}{2z=6}$.\\
Enfin, multiplier par $-1$ la deuxième ligne (c'est à dire changer tous les signes) et diviser par 2 la troisième ne change pas les solutions du système, si bien que 
$(S_1)$ équivaut à $\syst{x+y+z=6 }{y+3z=11}{z=3}$.\\
On retrouve bien le système ``facile'' précédent.\\
\textbf{Ainsi la solution du premier système est aussi le triplet $(1~;~2~;~3)$.\\}

Retenons donc les opérations élémentaires ``autorisées'' sur un tel système :
\renewcommand\labelitemi{\textbullet}
\begin{itemize}
\item L'interversion de deux lignes, notée : $L_i\leftrightarrow L_j$ ;
\item La multiplication d'une ligne par une constante $k\neq 0$, notée : $L_i\rightarrow kL_i$;
\item L'addition d'un multiple d'une ligne à une autre ligne, notée : $L_i \rightarrow L_i+kL_j$.
\end{itemize}
\pagebreak

Afin d'être plus synthétique, nous allons résumer la résolution précédente en la présentant sous forme d'une série de \textit{tableaux} :

\textbf{Tableau 1 (le système de départ)}
\begin{tabular}{|c|c|c|c|}
\hline ~~~2~~~ &~~~ 1 ~~~& ~~~$-1$ ~~~& ~~~1 ~~~\\ 
\hline 1 & 1 & 1 & 6 \\ 
\hline $-1$& $-1$ & 1 & 0 \\ 
\hline 
\end{tabular} 

\bigskip
\textbf{Tableau 2}
\begin{tabular}{|c|c|c|c|c|}
\hline \textbf{1} & 1 & 1 & 6&$L_1\leftrightarrow L_2$ \\ 
\hline ~~~2~~~ &~~~ 1 ~~~& ~~~$-1$ ~~~& ~~~1 ~~~&\\ 
\hline $-1$& $-1$ & 1 & 0& \\ 
\hline 
\end{tabular} 

\bigskip
\textbf{Tableau 3}
\begin{tabular}{|c|c|c|c|c|}
\hline 1 & 1 & 1 & 6& \\ 
\hline ~~~0~~~ &~~~ $-1$ ~~~& ~~~$-3$ ~~~& ~~~$-11$ ~~~~&$L_2\rightarrow L_2-2L_1$\\ 
\hline 0& 0 & 2 & 6 ~~~~&$L_3\rightarrow L_3+L_1$ \\ 
\hline 
\end{tabular} 


\bigskip
\textbf{Tableau 4}
\begin{tabular}{|c|c|c|c|c|}
\hline 1 & 1 & 1 & 6& \\ 
\hline ~~~0~~~ &~~~ $1$ ~~~& ~~~$3$ ~~~& ~~~$11$ ~~~~&$L_2\rightarrow -L_2$\\ 
\hline 0& 0 & 1 & 3 ~~~~&$L_3\rightarrow L_3/2$ \\ 
\hline 
\end{tabular} \\

Ce dernier tableau, ``triangulaire'', correspond à un système ``facile'' : on termine alors par les substitutions à rebours : connaissant $z$, on calcule $y$ puis connaissant $y$, on calcule $x$.\\

La méthode générale est donc la suivante :
\renewcommand\labelitemi{\textbullet}
\begin{itemize}
\item On écrit le tableau initial.
\item On fait en sorte (grâce aux opérations élémentaires autorisées) que le premier élément de la première ligne soit 1 (le \textbf{pivot}).
\item Par des opérations élémentaires, on fait en sorte que les nombres de la première colonne situés sous le pivot soient tous des 0.
\item On ne touche plus alors à la première ligne. On itère le procédé avec la deuxième ligne : on fait en sorte que le premier terme non nul de la deuxième ligne soit 1 (nouveau \textbf{pivot}) et tous ceux situés en dessous soient des 0.
\item Le tableau obtenu à ce stade est nécessairement ``triangulaire'' et correspond à un système ``facile''.
\item On termine la résolution par des \textit{substitutions à rebours}.
\end{itemize}

La méthode du pivot de Gauss s'applique bien sûr aussi aux systèmes linéaires $2-2$, et se généralise aussi aux systèmes linéaires $n-n$.

\pagebreak

Voici un autre exemple résolu par la méthode du pivot de Gauss, et présenté sous forme de tableaux :\\
\begin{exemple}

Résoudre le système $(S)$ :
$\syst{x+2y-3z=0}{2x-y+4z=5 }{3x-2y-z=0}$ 

\textbf{Tableau 1 (le système de départ)}
\begin{tabular}{|c|c|c|c|}
\hline ~~~\textbf{1}~~~ &~~~ 2 ~~~& ~~~$-3$ ~~~& ~~~0 ~~~\\ 
\hline 2 & $-1$ & 4 & 5 \\ 
\hline $3$& $-2$ & $-1$ & 0 \\ 
\hline 
\end{tabular} 

\bigskip
\textbf{Tableau 2}
\begin{tabular}{|c|c|c|c|c|}
\hline 1 & 2 & $-3$ & 0&\\ 
\hline ~~~0~~~ &~~~ $-5$ ~~~& ~~~$10$ ~~~& ~~~$5$ ~~~&$L_2\rightarrow L_2-2L_1$\\ 
\hline $0$& $-8$ & 8 & 0& $L_3\rightarrow L_3-3L_1$\\ 
\hline 
\end{tabular} 

\bigskip
\textbf{Tableau 3}
\begin{tabular}{|c|c|c|c|c|}
\hline 1 & 2 & $-3$ & 0&\\ 
\hline ~~~0~~~ &~~~ 1\textbf{} ~~~& ~~~$-2$ ~~~& ~~~$-1$ ~~~&$L_2\rightarrow L_2/(-5)$\\ 
\hline $0$& $-8$ & 8 & 0& \\ 
\hline 
\end{tabular} 


\bigskip
\textbf{Tableau 4}
\begin{tabular}{|c|c|c|c|c|}
\hline 1 & 2 & $-3$ & 0&\\ 
\hline ~~~0~~~ &~~~ 1 ~~~& ~~~$-2$ ~~~& ~~~$-1$ ~~~&\\ 
\hline 0& 0 & $-8$ & $-8$ ~~~~&$L_3\rightarrow L_3+8L_2$ \\ 
\hline 
\end{tabular} 

On obtient alors le système ``facile'' : $\syst{x+2y-3z=0}{y-2z=-1 }{-8z=-8}$ \\
La dernière équation donne $z=1$.\\
La deuxième donne alors $y=-1+2\times 1 = 1$.\\
La première donne enfin $x=-2\times 1 + 3 \times 1 = 1$.\\
\textbf{Ainsi la solution du système $(S)$ est le triplet $(1~;~1~;~1)$.}
\end{exemple}




\end{document}
