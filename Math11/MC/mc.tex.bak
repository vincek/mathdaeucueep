\documentclass[A4,12pt]{cueep}
\titre{machine à complexe}
\newdimen{\ll}
\usepackage{tikz}
\renewcommand{\SFC}{\includegraphics[height=8mm]{../../logo_lille1}\includegraphics[height=8mm]{../../ccbyncsa}}
\usetikzlibrary{arrows}

\usepackage{dot2texi}
\setlength{\ll}{60mm}
\reference{Math 11}

\begin{document}
\begin{enumerate}
\item Télécharger le fichier Geogebra \texttt{mc.ggb} sur passeport. À
  l'aide de Geogebra trouver pour chacune des neuf situations
  suivantes les <<opérations complexes>> qui permettent de transformer
  le triangle ABC en A'B'C'. Les règles de transformations sont les
  suivantes:

  \begin{Itemize}

  \item $+a$: translation de vecteur $a\overrightarrow{\imath}$.
  \item $+b\i$: translation de vecteur $b\overrightarrow{\jmath}$.
  \item $\times k$: homothétie (agrandissement/réduction) de rapport
    $k$.
  \item $\times \i$: rotation d'un quart de tour direct.
  \end{Itemize}
\vspace{5mm}


  \hspace{-10mm}\begin{tabular}[h]{ccc}
    \includegraphics[width=\ll]{mc_fig11}& \includegraphics[width=\ll]{mc_fig3}&\includegraphics[width=\ll]{mc_fig13}\\
    \includegraphics[width=\ll]{mc_fig4}& \includegraphics[width=\ll]{mc_fig15}&\includegraphics[width=\ll]{mc_fig6}\\
    \includegraphics[width=\ll]{mc_fig17}& \includegraphics[width=\ll]{mc_fig8}&\includegraphics[width=\ll]{mc_fig19}\\

  \end{tabular}
\pagebreak
\item 
  Dans tous les exemples précédents on <<passe>> de ABC à A'B'C' par
  plusieurs opérateurs complexes. Résumer chaque situation par la
  succession d'un opérateur multiplicatif et d'un opérateur additif.
  % \begin{center}
  %   \begin{tikzpicture}[>=latex',scale=0.5]
  %     \begin{dot2tex}[dot,tikz,codeonly,styleonly,options=-s -tmath]
  %       digraph G { node [shape='circle',style="fill=green!20"];
  %       rankdir=LR;
      
  %       LR_0[label="$\quad\quad$"]; LR_1[label="$\quad\quad$"];
  %       LR_2[label="$\quad\quad$"];
	
  %       LR_0 -> LR_1 [ label = "$\times$" ]; LR_1 -> LR_2 [ label =
  %       "+" ]; }
  %     \end{dot2tex}
  %   \end{tikzpicture}
  % \end{center}
 

  % \begin{tikzpicture}[-latex,auto,node distance=4cm,on
  %   grid,semithick ,state/.style={circle,top color=white,bottom
  %   color=processblue!20,draw, processblue ,circular drop
  %   shadow,text=blue,minimum width=1cm}]
  \begin{center}
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node
      distance=3cm,
      thick,state/.style={circle,fill=blue!20,draw,font=\sffamily\Large\bfseries}]

      \node[state] (A) {$\quad$}; \node[state] (B) [ right of=A]
      {$\quad$};

      \node[state] (C) [ right of=B] {$\quad$};

      \path(A) edge[bend left=25] node [above]{$\times$} (B); \path(B)
      edge [bend left=25 ] node [above]{$+$} (C);


    \end{tikzpicture}
  \end{center}

  Par exemple, pour la première figure, la transformation complexe qui
  permet de passer de ABC à A'B'C' est:
  \begin{center}
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node
      distance=3cm,
      thick,state/.style={circle,fill=blue!20,draw,font=\sffamily\Large\bfseries}]

      \node[state] (A) {$\quad$}; \node[state] (B) [ right of=A]
      {$\quad$};

      \node[state] (C) [ right of=B] {$\quad$};

      \path(A) edge[bend left=25] node [above]{$\times 1$} (B);
      \path(B) edge [bend left=25 ] node [above]{$+ (5+2\textrm{i})$}
      (C);


    \end{tikzpicture}
  \end{center}
\item Retrouver les résultats précédents par le calcul pour les
  situations encadrées.

Par exemple pour la première figure, on cherche la transformation complexe qui
  permet de passer de ABC à A'B'C' sous la forme $z\longmapsto az+b$.


  \begin{eqnarray*}
    \syst{a(-3-\i)+b=2+ \i}{a\times (-1)+b=4+2\i}{a(-3-3\i)+b=2+5\i}\\
     \syst{a(-3-\i)+b=2+ \i}{a\times (-1)+b=4+2\i}{a(-2+3\i)=-2+3\i}\\
 \syst{a(-3-\i)+b=2+ \i}{a\times (-1)+b=4+2\i}{a=1}\\
\sys{b=5+2\i}{a=1}\\
      \end{eqnarray*}


Donc la transformation complexe qui
  permet de passer de ABC à A'B'C' est $z\longmapsto z+5+2\i$.

Comment pouvait-on simplifier les calculs?
\end{enumerate}



\end{document}
