settings.outformat="pdf";
import graph;
import math;
import unicode;
settings.tex="pdflatex";

settings.render = 5;
//usepackage("[utf8]{inputenc}");
usepackage("kpfonts");
unitsize(0.7
	 cm,0.7cm);
limits((0,0),(18,24));
defaultpen(fontsize(11pt));
//size(300,175,IgnoreAspect);

draw ((0,12)--(18,12),linewidth(0.5bp));
draw ((0,0)--(0,24)--(18,24)--(18,0)--cycle,linewidth(0.5bp));

pen thin=linewidth(0.125*linewidth());
//xaxis("$x$",BottomTop,LeftTicks(begin=false,end=false,extend=true,ptick=thin));
xaxis(BottomTop,p=0.25bp+0.8orange,LeftTicks("%",extend=true,Step=1));
yaxis(BottomTop,p=0.25bp+0.8blue  +dashed,LeftTicks("%",extend=true,Step=0.4));
//xaxis("$M/M_\odot$",BottomTop,LeftTicks(DefaultFormat,new real[] {6,10,12,14,16,18}));



int[] p={2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,50,60,70,80,90,100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,910,920,930,940,945,950,955,960,965,970,975,980,985,990,991,992,993,994,995,996,997,998};
int l=p.length;

real f(real x) {return (1+erf(x/sqrt(2)))/2;};

real fp(real x) {return exp(-x^2/2)/sqrt(2*pi);};

real[] y;
real[] z;
for(int i=0; i<p.length;++i)
  {
    real fa(real x) {return f(x)-p[i]/1000;};
    real a=newton(100,fa,fp,-3,3);
    y.push(a);
    z.push(4*a+12);
  }
//write(y);
yaxis(BottomTop,p=0.25bp+0.8orange,LeftTicks("%",extend=true,z));


// points sur les axes verticaux
pen monpoint=scale(0.2mm)*currentpen;

for(int i=0;i<p.length;i=i+2)
  {
    dot(format("%d",p[i]),(0,z[i]),W,monpoint);
  }

real ly[]={-3,-2.5,-2,-1.5,-1,-0.5,0,0.5,1,1.5,2,2.5,3};



for(int i=0;i<ly.length;++i)
  {
    dot(format("%g",ly[i]),(18,4*ly[i]+12),E,monpoint);
  }


//label("Papier Gausso-arithmétique",(9,25),S);
//label("Fréquences cumulées en millième",(-0.5,24.5),SE);


marker croix=marker(scale(8)*cross(4),.6bp+blue);



real[] xi={43,44,45,46,47,48,49,50,51,52,53,54,55,56};
real[] yi={0.002739726,0.0082191781,0.0191780822,0.0438356164,0.0904109589,0.197260274,0.3260273973,0.5150684932,0.6931506849,0.8109589041,0.9205479452,0.9698630137,0.9890410959,0.9945205479};


real[] zi;

for(int i=0;i<yi.length;++i)
  {
   
    real fa(real x) {return f(x)-yi[i];};


    
    real a=newton(100,fa,fp,-3,3);
    
    zi.push(4*a+12);
  }
path polygone=graph(xi-42,zi);
dot(polygone,4bp+0.6blue);


//labels sur l'axe des abscisses

dot("52",(10,0),S,monpoint);
dot("50",(8,0),S,monpoint);
dot("43",(1,0),S,monpoint);


dot("Production en litre",(16,0),S,scale(0.01mm)*currentpen);

//droite

draw ((1,zi[0])--(14,zi[13]),linewidth(0.5bp)+red);


