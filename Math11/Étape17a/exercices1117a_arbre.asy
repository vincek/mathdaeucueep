// préambule asymptote
usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
usepackage("icomma");
//unitsize(0.0125cm);
//import fontsize;
defaultpen(fontsize(12pt));

settings.tex="pdflatex";





// code figure
real treeNodeStep = 0.3cm;
real treeLevelStep = 2.2cm;
real treeMinNodeHeight = 0.6cm;




struct TreeNode 
{
TreeNode parent;
TreeNode[] children;
frame content;
string prob;
pair pos;
real adjust;
bool labelpos;
  pen crayon;
}

void add( TreeNode child, TreeNode parent )
{
child.parent = parent;
parent.children.push( child );
}

TreeNode makeNode( TreeNode parent = null, frame f, string p, bool l,pen crayon )
{
TreeNode child = new TreeNode;
child.content = f;
child.prob = p;
child.labelpos=l;
child.crayon=crayon;
if( parent != null ) {add( child, parent );}
return child;
}

TreeNode makeNode( TreeNode parent = null, Label label, string p, bool l,pen crayon=black )
{
frame f;
label(f,label);
return makeNode( parent, f,p,l,crayon );
}

real layout( int level, TreeNode node )
{
real maxp=0;
real minp=1e10;
if( node.children.length > 0 ) 
  {
  real height[] = new real[node.children.length];
  real curHeight = 0;
  for( int i=node.children.length-1;i>=0; --i ) 
    {
    height[i] = layout( level+1, node.children[i] );
    node.children[i].pos = (level*treeLevelStep,curHeight + height[i]/2);
    maxp=max(maxp,node.children[i].pos.y);
    minp=min(minp,node.children[i].pos.y);
    curHeight += height[i] + treeNodeStep;
    }
  real midPoint=(maxp+minp)/2;
  for( int i=node.children.length-1;i>=0; --i ) 
    {
    node.children[i].adjust = - midPoint;
    }
  return max( (max(node.content)-min(node.content)).y,sum(height)+treeNodeStep*(height.length-1) );
  }
else {return max( treeMinNodeHeight, (max(node.content)-min(node.content)).y );}
}

void drawAll( TreeNode node, frame f )
{
pair pos;
if( node.parent != null ) pos = (0,node.parent.pos.y+node.adjust);
else pos = (0,node.adjust);
node.pos += pos;
node.content = shift(node.pos)*node.content;
add( f, node.content );
string proba=node.prob;
bool arrow=(find(proba,"@",0)!=-1);
if (arrow) proba=replace(proba,"@","");
if( node.parent != null ) 
  {
  path p = point(node.content, W)--point(node.parent.content,E);
  if (arrow) draw(p, currentpen,BeginArrow(2mm));
  else draw(p, currentpen+node.crayon);
  if( node.prob != "" ) 
  {
  if (node.labelpos) draw(Label(scale(.8)*proba),p,N);
  else draw(Label(scale(.8)*proba),p,S);
  }
  else 
  {
    if (arrow) draw(p, currentpen,BeginArrow(2mm) );
    else draw(p, currentpen+node.crayon);
  }
  }

for( int i = 0; i < node.children.length; ++i ) drawAll( node.children[i], f );
}

void draw( TreeNode root, pair pos )
{
frame f;
root.pos = (0,0);
layout( 1, root );
drawAll( root, f );
add(f,pos);
}





TreeNode root = makeNode("","",true);
TreeNode noeud1=makeNode(root,"$A_1$","$0,5$",true);
TreeNode noeud2=makeNode(root,"$\overline{A_1}$","$0,5$",false);


TreeNode noeud4=makeNode(noeud1,"$A_2$","$0,75$",true);
TreeNode noeud5=makeNode(noeud1,"$\overline{A_2}$","$0,25$",false);

TreeNode noeud6=makeNode(noeud2,"$A_2$","$0,5$",true);
TreeNode noeud7=makeNode(noeud2,"$\overline{A_2}$","$0,5$",false);




draw(root,(0,0));


