
// préambule asymptote
usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
usepackage("icomma");
// code figure
import graph;
import patterns;
unitsize(0.3cm,0.01cm);
xlimits(0,105);
ylimits(0,1500);
path rectangle0=((0,0)--(0,1083.4)--(20,1083.4)--(20,0)--cycle);
add("hachure0",hatch(3mm,magenta));
filldraw(rectangle0,pattern("hachure0"),1bp+0.7magenta);
path rectangle1=((20,0)--(20,1035.906)--(40,1035.906)--(40,0)--cycle);
add("hachure1",crosshatch(3mm,heavygray));
filldraw(rectangle1,pattern("hachure1"),1bp+0.7heavygray);
path rectangle2=((40,0)--(40,1063.839)--(60,1063.839)--(60,0)--cycle);
add("hachure2",hatch(3mm,magenta));
filldraw(rectangle2,pattern("hachure2"),1bp+0.7magenta);
path rectangle3=((60,0)--(60,744.784)--(75,744.784)--(75,0)--cycle);
add("hachure3",crosshatch(3mm,heavygray));
filldraw(rectangle3,pattern("hachure3"),1bp+0.7heavygray);
path rectangle4=((75,0)--(75,211.066)--(105,211.066)--(105,0)--cycle);
add("hachure4",hatch(3mm,magenta));
filldraw(rectangle4,pattern("hachure4"),1bp+0.7magenta);
xaxis(BottomTop,xmin=0,xmax=105,Ticks("%",extend=true,Step=5),p=linewidth(0.5pt)+gray(0.4));
yaxis(LeftRight,ymin=0,ymax=1500,Ticks("%",extend=true,Step=200),p=linewidth(0.5pt)+gray(0.4));
xaxis(axis=YEquals(0),xmin=0,xmax=105,Ticks(scale(1.2)*Label(),beginlabel=true,endlabel=true,begin=true,end=true,Step=10,Size=1mm),p=linewidth(1pt)+black,true);
yaxis(axis=XEquals(0),ymin=0,ymax=1500,Ticks("%",NoZero,Step=-21474.83648,Size=1mm),p=linewidth(1pt)+black,true);
// code supplémentaire


path rectangle5=((80,1000)--(80,1200)--(85,1200)--(85,1000)--cycle);
filldraw(rectangle5,pattern("hachure1"),1bp+0.7heavygray);
label(" 270\, 850 habitants",(95,1100),fontsize(18pt));
label(" âge en année",(105,30),fontsize(16pt));


shipout(bbox(0.1cm,0.1cm,white));




