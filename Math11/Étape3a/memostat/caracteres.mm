<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Population" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1424359479932"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="1"/>
<hook URI="peoplesilhouettes.png" SIZE="0.39601725" NAME="ExternalObject"/>
<node TEXT="caract&#xe8;res" LOCALIZED_STYLE_REF="styles.topic" POSITION="right" ID="ID_287449689" CREATED="1424358515176" MODIFIED="1424359646319" HGAP="60">
<edge STYLE="sharp_bezier" COLOR="#660066" WIDTH="2"/>
<font SIZE="16"/>
<node TEXT="caract&#xe8;res quantitatifs" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_939093732" CREATED="1424358533354" MODIFIED="1424359635791" HGAP="50" VSHIFT="-180">
<font SIZE="14"/>
<node TEXT="quantitatifs discrets" LOCALIZED_STYLE_REF="styles.subsubtopic" ID="ID_59184655" CREATED="1424358568866" MODIFIED="1424359655183" HGAP="30" VSHIFT="-110">
<font SIZE="12"/>
<node TEXT="L&apos;&#xe2;ge" ID="ID_1238258595" CREATED="1424359040255" MODIFIED="1424359449099" HGAP="30" VSHIFT="80">
<hook URI="dstankie-Birthday-cake.png" SIZE="0.16771573" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="quantitatifs continus" LOCALIZED_STYLE_REF="styles.subsubtopic" ID="ID_1285465721" CREATED="1424358592144" MODIFIED="1424359658703">
<font SIZE="12"/>
<node TEXT="Le poids" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1432696123" CREATED="1424359014879" MODIFIED="1424359450866" HGAP="50" VSHIFT="90">
<hook URI="balance.png" SIZE="0.19256866" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="caract&#xe8;res qualitatifs" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_931249361" CREATED="1424358551377" MODIFIED="1424359642415" HGAP="50">
<font SIZE="14"/>
<node TEXT="La couleur des yeux" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1186198233" CREATED="1424358809777" MODIFIED="1424359454304" HGAP="30" VSHIFT="70">
<hook URI="secretlondon-Blue-eye.png" SIZE="0.2487078" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
</map>
