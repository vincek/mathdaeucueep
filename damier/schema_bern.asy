// préambule asymptote
usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
usepackage("icomma");
//unitsize(0.0125cm);
//import fontsize;
defaultpen(fontsize(14pt));

import animation;

settings.tex="pdflatex";


animation A=animation("schema_bern_", global=false);


// code figure
real treeNodeStep = 0.3cm;
real treeLevelStep = 2.2cm;
real treeMinNodeHeight = 0.6cm;




struct TreeNode 
{
TreeNode parent;
TreeNode[] children;
frame content;
string prob;
pair pos;
real adjust;
bool labelpos;
  pen crayon;
}

void add( TreeNode child, TreeNode parent )
{
child.parent = parent;
parent.children.push( child );
}

TreeNode makeNode( TreeNode parent = null, frame f, string p, bool l,pen crayon )
{
TreeNode child = new TreeNode;
child.content = f;
child.prob = p;
child.labelpos=l;
child.crayon=crayon;
if( parent != null ) {add( child, parent );}
return child;
}

TreeNode makeNode( TreeNode parent = null, Label label, string p, bool l,pen crayon=black )
{
frame f;
label(f,label);
return makeNode( parent, f,p,l,crayon );
}

real layout( int level, TreeNode node )
{
real maxp=0;
real minp=1e10;
if( node.children.length > 0 ) 
  {
  real height[] = new real[node.children.length];
  real curHeight = 0;
  for( int i=node.children.length-1;i>=0; --i ) 
    {
    height[i] = layout( level+1, node.children[i] );
    node.children[i].pos = (level*treeLevelStep,curHeight + height[i]/2);
    maxp=max(maxp,node.children[i].pos.y);
    minp=min(minp,node.children[i].pos.y);
    curHeight += height[i] + treeNodeStep;
    }
  real midPoint=(maxp+minp)/2;
  for( int i=node.children.length-1;i>=0; --i ) 
    {
    node.children[i].adjust = - midPoint;
    }
  return max( (max(node.content)-min(node.content)).y,sum(height)+treeNodeStep*(height.length-1) );
  }
else {return max( treeMinNodeHeight, (max(node.content)-min(node.content)).y );}
}

void drawAll( TreeNode node, frame f )
{
pair pos;
if( node.parent != null ) pos = (0,node.parent.pos.y+node.adjust);
else pos = (0,node.adjust);
node.pos += pos;
node.content = shift(node.pos)*node.content;
add( f, node.content );
string proba=node.prob;
bool arrow=(find(proba,"@",0)!=-1);
if (arrow) proba=replace(proba,"@","");
if( node.parent != null ) 
  {
  path p = point(node.content, W)--point(node.parent.content,E);
  if (arrow) draw(p, currentpen,BeginArrow(2mm));
  else draw(p, currentpen+node.crayon);
  if( node.prob != "" ) 
  {
  if (node.labelpos) draw(Label(scale(.8)*proba),p,N);
  else draw(Label(scale(.8)*proba),p,S);
  }
  else 
  {
    if (arrow) draw(p, currentpen,BeginArrow(2mm) );
    else draw(p, currentpen+node.crayon);
  }
  }

for( int i = 0; i < node.children.length; ++i ) drawAll( node.children[i], f );
}

void draw( TreeNode root, pair pos )
{
frame f;
root.pos = (0,0);
layout( 1, root );
drawAll( root, f );
add(f,pos);
}





TreeNode root = makeNode("","",true);
TreeNode noeud1=makeNode(root,"$C_1$","$0,54$",true);
TreeNode noeud2=makeNode(root,"$\overline{C_1}$","$0,46$",false);


TreeNode noeud4=makeNode(noeud1,"$C_2$","$0,54$",true);
TreeNode noeud5=makeNode(noeud1,"$\overline{C_2}$","$0,46$",false);

TreeNode noeud6=makeNode(noeud2,"$C_2$","$0,54$",true);
TreeNode noeud7=makeNode(noeud2,"$\overline{C_2}$","$0,46$",false);



TreeNode noeud8=makeNode(noeud6,"$C_3$","$0,54$",true);
TreeNode noeud9=makeNode(noeud6,"$\overline{C_3}$","$0,46$",false);

TreeNode noeud10=makeNode(noeud7,"$C_3$","$0,54$",true);
TreeNode noeud11=makeNode(noeud7,"$\overline{C_3}$","$0,46$",false);

TreeNode noeud12=makeNode(noeud4,"$C_3$","$0,54$",true);
TreeNode noeud13=makeNode(noeud4,"$\overline{C_3}$","$0,46$",false);

TreeNode noeud14=makeNode(noeud5,"$C_3$","$0,54$",true);
TreeNode noeud15=makeNode(noeud5,"$\overline{C_3}$","$0,46$",false);



TreeNode noeud16=makeNode(noeud15,"$C_4$","$0,54$",true);
TreeNode noeud17=makeNode(noeud15,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud18=makeNode(noeud14,"$C_4$","$0,54$",true);
TreeNode noeud19=makeNode(noeud14,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud20=makeNode(noeud13,"$C_4$","$0,54$",true);
TreeNode noeud21=makeNode(noeud13,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud22=makeNode(noeud12,"$C_4$","$0,54$",true);
TreeNode noeud23=makeNode(noeud12,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud24=makeNode(noeud11,"$C_4$","$0,54$",true);
TreeNode noeud31=makeNode(noeud11,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud25=makeNode(noeud10,"$C_4$","$0,54$",true);
TreeNode noeud26=makeNode(noeud10,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud27=makeNode(noeud9,"$C_4$","$0,54$",true);
TreeNode noeud28=makeNode(noeud9,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud29=makeNode(noeud8,"$C_4$","$0,54$",true);
TreeNode noeud30=makeNode(noeud8,"$\overline{C_4}$","$0,46$",false);
draw(root,(0,0));
A.add();
erase();

// deuxième figure


TreeNode root = makeNode("","",true);
TreeNode noeud1=makeNode(root,"$C_1$","$0,54$",true);
TreeNode noeud2=makeNode(root,"$\overline{C_1}$","$0,46$",false,red+linewidth(2));


TreeNode noeud4=makeNode(noeud1,"$C_2$","$0,54$",true);
TreeNode noeud5=makeNode(noeud1,"$\overline{C_2}$","$0,46$",false);

TreeNode noeud6=makeNode(noeud2,"$C_2$","$0,54$",true);
TreeNode noeud7=makeNode(noeud2,"$\overline{C_2}$","$0,46$",false,red+linewidth(2));



TreeNode noeud8=makeNode(noeud6,"$C_3$","$0,54$",true);
TreeNode noeud9=makeNode(noeud6,"$\overline{C_3}$","$0,46$",false);

TreeNode noeud10=makeNode(noeud7,"$C_3$","$0,54$",true);
TreeNode noeud11=makeNode(noeud7,"$\overline{C_3}$","$0,46$",false,red+linewidth(2));


TreeNode noeud12=makeNode(noeud4,"$C_3$","$0,54$",true);
TreeNode noeud13=makeNode(noeud4,"$\overline{C_3}$","$0,46$",false);

TreeNode noeud14=makeNode(noeud5,"$C_3$","$0,54$",true);
TreeNode noeud15=makeNode(noeud5,"$\overline{C_3}$","$0,46$",false);



TreeNode noeud16=makeNode(noeud15,"$C_4$","$0,54$",true);
TreeNode noeud17=makeNode(noeud15,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud18=makeNode(noeud14,"$C_4$","$0,54$",true);
TreeNode noeud19=makeNode(noeud14,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud20=makeNode(noeud13,"$C_4$","$0,54$",true);
TreeNode noeud21=makeNode(noeud13,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud22=makeNode(noeud12,"$C_4$","$0,54$",true);
TreeNode noeud23=makeNode(noeud12,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud24=makeNode(noeud11,"$C_4$","$0,54$",true);
TreeNode noeud31=makeNode(noeud11,"$\overline{C_4}$","$0,46$",false,red+linewidth(2));

TreeNode noeud25=makeNode(noeud10,"$C_4$","$0,54$",true);
TreeNode noeud26=makeNode(noeud10,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud27=makeNode(noeud9,"$C_4$","$0,54$",true);
TreeNode noeud28=makeNode(noeud9,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud29=makeNode(noeud8,"$C_4$","$0,54$",true);
TreeNode noeud30=makeNode(noeud8,"$\overline{C_4}$","$0,46$",false);
draw(root,(0,0));
A.add();


erase();

// troisième  figure


TreeNode root = makeNode("","",true);

TreeNode noeud1=makeNode(root,"$C_1$","$0,54$",true,green+linewidth(2));
TreeNode noeud2=makeNode(root,"$\overline{C_1}$","$0,46$",false,red+linewidth(2));




TreeNode noeud4=makeNode(noeud1,"$C_2$","$0,54$",true,green+linewidth(2));
TreeNode noeud5=makeNode(noeud1,"$\overline{C_2}$","$0,46$",false,red+linewidth(2));

TreeNode noeud6=makeNode(noeud2,"$C_2$","$0,54$",true,green+linewidth(2));
TreeNode noeud7=makeNode(noeud2,"$\overline{C_2}$","$0,46$",false,red+linewidth(2));


TreeNode noeud8=makeNode(noeud6,"$C_3$","$0,54$",true,green+linewidth(2));
TreeNode noeud9=makeNode(noeud6,"$\overline{C_3}$","$0,46$",false,red+linewidth(2));


TreeNode noeud10=makeNode(noeud7,"$C_3$","$0,54$",true,green+linewidth(2));
TreeNode noeud11=makeNode(noeud7,"$\overline{C_3}$","$0,46$",false);





TreeNode noeud12=makeNode(noeud4,"$C_3$","$0,54$",true);
TreeNode noeud13=makeNode(noeud4,"$\overline{C_3}$","$0,46$",false,red+linewidth(2));

TreeNode noeud14=makeNode(noeud5,"$C_3$","$0,54$",true,green+linewidth(2));
TreeNode noeud15=makeNode(noeud5,"$\overline{C_3}$","$0,46$",false,red+linewidth(2));



TreeNode noeud16=makeNode(noeud15,"$C_4$","$0,54$",true,green+linewidth(2));
TreeNode noeud17=makeNode(noeud15,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud18=makeNode(noeud14,"$C_4$","$0,54$",true);
TreeNode noeud19=makeNode(noeud14,"$\overline{C_4}$","$0,46$",false,red+linewidth(2));

TreeNode noeud20=makeNode(noeud13,"$C_4$","$0,54$",true);
TreeNode noeud21=makeNode(noeud13,"$\overline{C_4}$","$0,46$",false,red+linewidth(2));

TreeNode noeud22=makeNode(noeud12,"$C_4$","$0,54$",true);
TreeNode noeud23=makeNode(noeud12,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud24=makeNode(noeud11,"$C_4$","$0,54$",true);
TreeNode noeud31=makeNode(noeud11,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud25=makeNode(noeud10,"$C_4$","$0,54$",true,green+linewidth(2));
TreeNode noeud26=makeNode(noeud10,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud27=makeNode(noeud9,"$C_4$","$0,54$",true,green+linewidth(2));
TreeNode noeud28=makeNode(noeud9,"$\overline{C_4}$","$0,46$",false);

TreeNode noeud29=makeNode(noeud8,"$C_4$","$0,54$",true);
TreeNode noeud30=makeNode(noeud8,"$\overline{C_4}$","$0,46$",false,red+linewidth(2));
draw(root,(0,0));
A.add();



erase();

// quatrième figure


TreeNode root = makeNode("","",true);

TreeNode noeud1=makeNode(root,"H","$0,5$",true);
TreeNode noeud2=makeNode(root,"D","$0,5$",false);




TreeNode noeud4=makeNode(noeud1,"H","$0,5$",true);
TreeNode noeud5=makeNode(noeud1,"D","$0,5$",false);

TreeNode noeud6=makeNode(noeud2,"H","$0,5$",true);
TreeNode noeud7=makeNode(noeud2,"D","$0,5$",false);


TreeNode noeud8=makeNode(noeud6,"H","$0,5$",true);
TreeNode noeud9=makeNode(noeud6,"D","$0,5$",false);


TreeNode noeud10=makeNode(noeud7,"H","$0,5$",true);
TreeNode noeud11=makeNode(noeud7,"D","$0,5$",false);





TreeNode noeud12=makeNode(noeud4,"H","$0,5$",true);
TreeNode noeud13=makeNode(noeud4,"D","$0,5$",false);

TreeNode noeud14=makeNode(noeud5,"H","$0,5$",true);
TreeNode noeud15=makeNode(noeud5,"D","$0,5$",false);



TreeNode noeud16=makeNode(noeud15,"H","$0,5$",true);
TreeNode noeud17=makeNode(noeud15,"D","$0,5$",false);

TreeNode noeud18=makeNode(noeud14,"H","$0,5$",true);
TreeNode noeud19=makeNode(noeud14,"D","$0,5$",false);

TreeNode noeud20=makeNode(noeud13,"H","$0,5$",true);
TreeNode noeud21=makeNode(noeud13,"D","$0,5$",false);

TreeNode noeud22=makeNode(noeud12,"H","$0,5$",true);
TreeNode noeud23=makeNode(noeud12,"D","$0,5$",false);

TreeNode noeud24=makeNode(noeud11,"H","$0,5$",true);
TreeNode noeud31=makeNode(noeud11,"D","$0,5$",false);

TreeNode noeud25=makeNode(noeud10,"H","$0,5$",true);
TreeNode noeud26=makeNode(noeud10,"D","$0,5$",false);

TreeNode noeud27=makeNode(noeud9,"H","$0,5$",true);
TreeNode noeud28=makeNode(noeud9,"D","$0,5$",false);

TreeNode noeud29=makeNode(noeud8,"H","$0,5$",true);
TreeNode noeud30=makeNode(noeud8,"D","$0,5$",false);
draw(root,(0,0));
A.add();


