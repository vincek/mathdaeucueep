import math;
unitsize(1cm,1cm);

int n=15;


path carre=(0,0)--(0,n)--(n,n)--(n,0)--cycle;


import patterns;



picture mychecker(real Hx=10mm, real Hy=0, pen p=currentpen)
{
  picture tiling;
  if(Hy == 0) Hy=Hx;
  path tile=box((0,0),(Hx,Hy));
  fill(tiling,tile,p);
  fill(tiling,shift(Hx,Hy)*tile,p);
  clip(tiling,box((0,0),(2Hx,2Hy)));
  return tiling;
}

add("checker",mychecker());

filldraw(carre,pattern("checker"));

//add(grid(n,n));

