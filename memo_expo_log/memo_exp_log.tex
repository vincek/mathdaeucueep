%%% Fichier mis à disposition selon la licence CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) %%%
\documentclass[A4,12pt]{cueep}
\renewcommand{\SFC}{\includegraphics[height=8mm]{../logo_lille1}\includegraphics[height=8mm]{../ccbyncsa}}
 
\titre{Logarithme et exponentielle}
\reference{Math 10\\EC2}
\begin{document}


\section{Les logarithmes}
\label{sec:flog}

\subsection{Définition}
\label{sec:logdef}
\begin{Def}
  L'unique primitive de la fonction inverse définie sur $]0;+\infty[$
  qui s'annule en 1 est appelée fonction logarithme népérien et est
  notée $\ln$.
\end{Def}


Conséquences directes:

\begin{Itemize}
\item  $\ln(1)=0$
\item La fonction $\ln$ est dérivable sur $]0;+\infty[$ et
  $\ln'(x)=\frac{1}{x}$
\item Comme sur $]0;+\infty[$, $\frac{1}{x}>0$, la fonction logarithme
  est strictement croissante.
\end{Itemize}





\begin{center}
  \includegraphics[width=13cm]{memo_log_exp_fig1}
\end{center}

L'unique antécédent de $1$ par la fonction $\ln$ est noté $\e$.

\begin{remarque}
  \begin{Itemize}
  \item 
    $\e\simeq 2,7182818284$
  \item Par définition, $\ln(\e)=1$
  \end{Itemize}

\end{remarque}

\subsection{Propriétés algébriques}


\begin{thm} Soient $a$ et $b$ deux réels positifs.
  $\ln(ab)=\ln(a)+\ln(b)$
\end{thm}


La fonction logarithme transforme les produits en sommes.
\pagebreak


\begin{pro}$\quad$\\
\begin{Itemize}
  \item $\ln(\frac{a}{b})=\ln(a)-\ln(b)$
  \item $\ln(\frac{1}{a})=-\ln(a)$
  \item $\ln(a^n)=n\ln(a)$
  \item $\ln(\sqrt{a})=\frac{1}{2}\ln(a)$
\end{Itemize}



\end{pro}



\begin{pro} Soient $X$ et $Y$ deux réels strictement positifs.
  \begin{equation*}
  X\leq Y \Leftrightarrow \ln(X)\leq \ln(Y)
\end{equation*}

\end{pro}
\begin{remarque}
  On a le même genre de résultat avec $<$; $>$; $\geq$ et $=$.
\end{remarque}


\subsection{Logarithme de base $a$}

Soit $a$ un  nombre réel strictement positif.

\begin{Def}
  On appelle logarithme de base $a$ la fonction définie sur
  $]0;+\infty[$ par

  \begin{equation*}
    \label{eq:loga}
    \log_a(x)=\frac{\ln(x)}{\ln(a)}
  \end{equation*}
\end{Def}


Lorsque $a=10$, on parle de logarithme décimal et on le note tout
simplement $\log$. Le logarithme décimal est très utilisé en science
(pH, notion de décibel, etc.).

Les propriétés algébriques de la fonction $\log_a$ sont les mêmes que celle de la
fonction $\ln$. Mais dans le cas du logarithme décimal, il est
intéressant de noter:

\begin{pro}
  Soit $n$ un entier relatif, $\log(10^n)=n$.



 De plus,  $y=\log(x) \Leftrightarrow x=10^y$  
\end{pro}

Remarquons que $\log(10)=1$; $\log(0,1)=-1$; $\log(100)=2$;  $\log(0,01)=-2$; $\log(1000)=3$


\section{L'exponentielle}

\subsection{Définition}
\label{sec:expdef}


Comme la fonction logarithme népérien est continue et strictement
croissante elle admet une fonction réciproque, c'est à dire que pour
tout nombre réel $y$, il existe un nombre $x$  (strictement positif) tel que $y=\ln(x)$, le
nombre $x$ sera noté $\exp(y)$. 


\begin{equation*}
  y=\ln(x)\Leftrightarrow x=\exp(y)
\end{equation*}


Si $(x;\ln(x))$ est un point de la courbe d'équation $Y=\ln(X)$ alors
$(\ln(x);x)$ est un point de la courbe d'équation $Y=\exp(X)$. Par
conséquent on obtient une courbe à partir de l'autre par une symétrie
d'axe $y=x$.

\begin{center}
  \includegraphics[width=13cm]{memo_log_exp_fig2}
\end{center}


Conséquences directes:

\begin{Itemize}

\item  $\exp(0)=1$
\item  La fonction exponentielle
  est strictement croissante.
\item Pour tout nombre réel $x$, $\exp(x)>0$
\item $\exp(1)=\e$
\item $\ln(\exp(x))=x$
\item Si $x>0$, $\exp(\ln(x))=x$
\end{Itemize}

\begin{thm}
  La fonction exponentielle est dérivable et sa dérivée c'est elle même.
\end{thm}


En d'autres termes: $\exp'(x)=\exp(x)$


\subsection{Propriétés algébriques}


\begin{thm}
   Soient $a$ et $b$ deux nombres réels.

\[\exp(a+b)=\exp(a)\times\exp(b)\]
\end{thm}


La fonction exponentielle transforme les sommes en produits.


\begin{cor} Soient $a$ et $b$ deux réels et $n$ un entier naturel.
  \begin{Itemize}
   \item $\exp(a-b)=\frac{\exp(a)}{\exp(b)}$
  \item $\exp(a\times n)=\exp(a)^n$
  \item $\exp(\frac{a}{2})=\sqrt{\exp(a)}$
   \end{Itemize}
\end{cor}





\begin{pro} Soient $X$ et $Y$ deux nombres réels.
  \begin{equation*}
  X\leq Y \Leftrightarrow \exp(X)\leq \exp(Y)
\end{equation*}

\end{pro}
\begin{remarque}
  On a le même genre de résultat avec $<$; $>$; $\geq$ et $=$.
\end{remarque}


\subsection{Fonction puissance}
\label{sec:foncpuis}

\begin{Def}
  Soit $a$ un réel strictement positif, on pose $a^x=\exp(x\ln(a))$ et
  l'on définit ainsi une fonction appelée fonction puissance de base $a$.
\end{Def}


Remarque:

\begin{equation*}
  \e^x=\exp(x\ln(\e))=\exp(x)
\end{equation*}

Les fonctions puissances vérifient les même propriétés algébriques que
la fonction exponentielle ($a^{x+y}=a^x\times a^y$, etc.).


Si $0<a<1$ alors la courbe d'équation $y=a^x$ est décroissante, si
$a>1$ elle est croissante.

Ci-dessous deux courbes représentant respectivement une augmentation de 3\% par an et
une diminution de 5\% par an.

\begin{center}
  \includegraphics[width=11cm]{memo_log_exp_fig3}
\end{center}





\section{Compléments sur la dérivation}
\label{sec:comder}

\subsection{Composition}
\label{sec:dercomp}


\begin{thm}
  Soit $u$ une fonction dérivable, $\exp(u)$ est dérivable et
  $u'\exp(u)$ est sa dérivée. Si de plus $u>0$, alors $\ln(u)$ est
  dérivable et $\frac{u'}{u}$ est sa dérivée.
\end{thm}


En particulier:


\begin{Itemize}
\item  $[\exp(ax+b)]'=a\exp(ax+b)$
\item Pour $x$ tel que $ax+b>0$, $[\ln(ax+b)]'=\frac{a}{ax+b}$ 
\end{Itemize}


\subsection{Fonction logarithme et exponentielle de base $a$}
\label{sec:derbasea}

\begin{pro} Soit $a$ un nombre réel strictement positif.
  \begin{Itemize}

  \item $[a^x]'=\ln(a)a^x$
  \item $[\log_a(x)]'=\frac{1}{x\ln(a)}$
  \end{Itemize}
\end{pro}
\end{document}
