if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="La_tuberculose-2";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);


usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
usepackage("icomma");
// code figure
real treeNodeStep = 0.5cm;
real treeLevelStep = 2.8cm;
real treeMinNodeHeight = 0.6cm;
struct TreeNode
{
TreeNode parent;
TreeNode[] children;
frame content;
string prob;
pair pos;
real adjust;
bool labelpos;
}




void add( TreeNode child, TreeNode parent )
{
child.parent = parent;
parent.children.push( child );
}

TreeNode makeNode( TreeNode parent = null, frame f, string p, bool l )
{
TreeNode child = new TreeNode;
child.content = f;
child.prob = p;
child.labelpos=l;
if( parent != null ) {add( child, parent );}
return child;
}

TreeNode makeNode( TreeNode parent = null, Label label, string p, bool l )
{
frame f;
label(f,label);
return makeNode( parent, f,p,l );
}

real layout( int level, TreeNode node )
{
real maxp=0;
real minp=1e10;
if( node.children.length > 0 )
{
real height[] = new real[node.children.length];
real curHeight = 0;
for( int i=node.children.length-1;i>=0; --i )
{
height[i] = layout( level+1, node.children[i] );
node.children[i].pos = (level*treeLevelStep,curHeight + height[i]/2);
maxp=max(maxp,node.children[i].pos.y);
minp=min(minp,node.children[i].pos.y);
curHeight += height[i] + treeNodeStep;
}
real midPoint=(maxp+minp)/2;
for( int i=node.children.length-1;i>=0; --i )
{
node.children[i].adjust = - midPoint;
}
return max( (max(node.content)-min(node.content)).y,sum(height)+treeNodeStep*(height.length-1) );
}
else {return max( treeMinNodeHeight, (max(node.content)-min(node.content)).y );}
}

void drawAll( TreeNode node, frame f )
{
pair pos;
if( node.parent != null ) pos = (0,node.parent.pos.y+node.adjust);
else pos = (0,node.adjust);
node.pos += pos;
node.content = shift(node.pos)*node.content;
add( f, node.content );
string proba=node.prob;
bool arrow=(find(proba,"@",0)!=-1);
if (arrow) proba=replace(proba,"@","");
if( node.parent != null )
{
path p = point(node.content, W)--point(node.parent.content,E);
if (arrow) draw(p, currentpen,BeginArrow(2mm) );
else draw(p, currentpen);
if( node.prob != "" )
{
if (node.labelpos) draw(Label(scale(.7)*proba),p,N);
else draw(Label(scale(.7)*proba),p,S);
}
else
{
if (arrow) draw(p, currentpen,BeginArrow(2mm) );
else draw(p, currentpen);
}
}
for( int i = 0; i < node.children.length; ++i ) drawAll( node.children[i], f );
}

void draw( TreeNode root, pair pos )
{
frame f;
root.pos = (0,0);
layout( 1, root );
drawAll( root, f );
add(f,pos);
}

TreeNode root = makeNode("","",true);
TreeNode noeud1=makeNode(root,"$U$","$0,7$",true);
TreeNode noeud4=makeNode(noeud1,"$C$","$0,8$",true);
TreeNode noeud11=makeNode(noeud4,"$\p(U\cap C)=0,56$\hphantom{00}","$@$",true);
TreeNode noeud5=makeNode(noeud1,"$\overline{C}$","$0,2$",false);
TreeNode noeud12=makeNode(noeud5,"$\p(U\cap \overline{C})=0,14$\hphantom{00}","$@$",true);
TreeNode noeud2=makeNode(root,"$D$","$0,15$",true);
TreeNode noeud6=makeNode(noeud2,"$C$","$0,95$",true);
TreeNode noeud13=makeNode(noeud6,"$\p(D\cap C)=0,1425$","$@$",true);
TreeNode noeud7=makeNode(noeud2,"$\overline{C}$","$0,05$",false);
TreeNode noeud14=makeNode(noeud7,"$\p(D\cap \overline{C})=0,0075$","$@$",true);
TreeNode noeud3=makeNode(root,"$T$","$0,15$",false);
TreeNode noeud8=makeNode(noeud3,"$C$","$0,96$",true);
TreeNode noeud15=makeNode(noeud8,"$\p(T\cap C)=0,144$\hphantom{0}","$@$",true);
TreeNode noeud10=makeNode(noeud3,"$\overline{C}$","$0,04$",false);
TreeNode noeud16=makeNode(noeud10,"$\p(T\cap \overline{C})=0,006$\hphantom{0}","$@$",true);
draw(root,(0,0));


