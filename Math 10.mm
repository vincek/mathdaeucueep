<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Math 10" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1384264508520">
<hook NAME="MapStyle" max_node_width="600"/>
<node TEXT="Utiliser les propri&#xe9;t&#xe9;s d&apos;une fonction pour d&#xe9;crire une situation concr&#xe8;te&#xa0;;" POSITION="right" ID="ID_95352573" CREATED="1384263764347" MODIFIED="1384264867162" HGAP="30" VSHIFT="-20">
<edge COLOR="#ff0000"/>
</node>
<node TEXT="reconna&#xee;tre et r&#xe9;soudre les situations probl&#xe8;mes n&#xe9;cessitant le recours &#xe0; l&apos;&#xe9;tude de fonctions polynomiales ou rationnelles (probl&#xe8;mes d&apos;optimisation, notamment)&#xa0;;" POSITION="right" ID="ID_38485242" CREATED="1384263764347" MODIFIED="1384265207893" VSHIFT="-40">
<edge COLOR="#0000ff"/>
<arrowlink COLOR="#000000" DESTINATION="ID_645478517" STARTINCLINATION="240;0;" ENDINCLINATION="-52;6;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="reconna&#xee;tre et utiliser le mod&#xe8;le d&#xe9;rivation-int&#xe9;gration dans des situations diverses (calculs d&apos;aires, de probabilit&#xe9;s, de pentes et de tangentes)&#xa0;;" POSITION="right" ID="ID_1698106281" CREATED="1384263764350" MODIFIED="1415624362978" VSHIFT="-40">
<edge COLOR="#00ff00"/>
<arrowlink COLOR="#000000" DESTINATION="ID_766327644" STARTINCLINATION="106;9;" ENDINCLINATION="-242;31;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<arrowlink COLOR="#000000" DESTINATION="ID_543015" STARTINCLINATION="242;-20;" ENDINCLINATION="55;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<arrowlink COLOR="#000000" DESTINATION="ID_353268164" STARTINCLINATION="235;5;" ENDINCLINATION="-38;19;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="ma&#xee;triser quelques concepts avanc&#xe9;s d&apos;analyse (calculs de limites, comportement asymptotique, continuit&#xe9;...)&#xa0;;" POSITION="right" ID="ID_805000103" CREATED="1384263764359" MODIFIED="1384265183727" VSHIFT="-40">
<edge COLOR="#ff00ff"/>
<arrowlink COLOR="#000000" DESTINATION="ID_536147522" STARTINCLINATION="305;0;" ENDINCLINATION="-104;-11;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<arrowlink COLOR="#000000" DESTINATION="ID_947016718" STARTINCLINATION="320;21;" ENDINCLINATION="-198;107;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<arrowlink COLOR="#000000" DESTINATION="ID_1524296346" STARTINCLINATION="307;12;" ENDINCLINATION="-141;33;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="utiliser les fonctions exponentielles et logarithme pour r&#xe9;soudre un probl&#xe8;me d&apos;&#xe9;volution&#xa0;;" POSITION="right" ID="ID_168213753" CREATED="1384263764390" MODIFIED="1384264882505" HGAP="30" VSHIFT="-40">
<edge COLOR="#00ffff"/>
</node>
<node TEXT="utiliser la calculatrice graphique et l&apos;outil informatique pour &#xe9;mettre des conjectures et v&#xe9;rifier la validit&#xe9; de certaines hypoth&#xe8;ses&#xa0;;" POSITION="right" ID="ID_828860948" CREATED="1384263764405" MODIFIED="1384264884196" VSHIFT="-30">
<edge COLOR="#ffff00"/>
</node>
<node POSITION="right" ID="ID_1885322460" CREATED="1384263818587" MODIFIED="1415624866427" HGAP="1661" VSHIFT="-123" LOCALIZED_STYLE_REF="AutomaticLayout.level.root">
<richcontent TYPE="NODE">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="6">Fiches</font>
    </p>
  </body>
</html>
</richcontent>
<edge COLOR="#ffffff" WIDTH="thin"/>
<hook NAME="FreeNode"/>
<node TEXT="EC1" ID="ID_1672156590" CREATED="1384263855758" MODIFIED="1415624892207" HGAP="-340" VSHIFT="-65">
<edge COLOR="#3333ff"/>
<cloud COLOR="#f0f0f0" WIDTH="0"/>
<node TEXT="La bou&#xe9;e" ID="ID_645478517" CREATED="1384264318202" MODIFIED="1384265333334" HGAP="-120">
<arrowlink COLOR="#000000" DESTINATION="ID_95352573" STARTINCLINATION="-128;-71;" ENDINCLINATION="298;27;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<icon BUILTIN="75%"/>
</node>
<node TEXT="La fl&#xfb;te de champagne" ID="ID_543015" CREATED="1384264318202" MODIFIED="1384265337984" HGAP="-110" VSHIFT="10">
<icon BUILTIN="75%"/>
</node>
<node TEXT="La voute et la rivi&#xe8;re" ID="ID_353268164" CREATED="1384264318203" MODIFIED="1384265347511" HGAP="-200" VSHIFT="20">
<icon BUILTIN="50%"/>
</node>
<node TEXT="Le d&#xe9;marrage" ID="ID_760568428" CREATED="1384264318205" MODIFIED="1415624639606" HGAP="-200" VSHIFT="30">
<icon BUILTIN="100%"/>
</node>
<node TEXT="Le puits" ID="ID_762434911" CREATED="1384264318206" MODIFIED="1384265470597" HGAP="-160" VSHIFT="10">
<arrowlink COLOR="#000000" DESTINATION="ID_646884962" STARTINCLINATION="-257;85;" ENDINCLINATION="541;104;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<icon BUILTIN="50%"/>
</node>
<node TEXT="Le ballon sonde" ID="ID_1664955215" CREATED="1384264318207" MODIFIED="1384265476181" HGAP="-150" VSHIFT="-30">
<edge COLOR="#3333ff"/>
<icon BUILTIN="75%"/>
</node>
<node TEXT="dragster" ID="ID_20782651" CREATED="1384264318208" MODIFIED="1415624477678" HGAP="-140" VSHIFT="-20">
<icon BUILTIN="100%"/>
</node>
<node TEXT="Les pneus" ID="ID_932882848" CREATED="1384264318209" MODIFIED="1384265485100" HGAP="-140" VSHIFT="-40">
<arrowlink COLOR="#000000" DESTINATION="ID_1698106281" STARTINCLINATION="-75;9;" ENDINCLINATION="264;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<icon BUILTIN="75%"/>
</node>
<node TEXT="La tuberculose" ID="ID_945367417" CREATED="1415624440789" MODIFIED="1415624460585">
<icon BUILTIN="100%"/>
</node>
<node TEXT="Le canal" ID="ID_950368770" CREATED="1415624584716" MODIFIED="1415624595028">
<icon BUILTIN="100%"/>
</node>
<node TEXT="La ville" ID="ID_520711323" CREATED="1415624654615" MODIFIED="1415624672208">
<icon BUILTIN="100%"/>
</node>
<node TEXT="Le TGV" ID="ID_1205832192" CREATED="1415624763823" MODIFIED="1415624774736">
<icon BUILTIN="100%"/>
</node>
</node>
<node TEXT="EC2" ID="ID_1694298040" CREATED="1384263865456" MODIFIED="1384265317666" HGAP="-150" VSHIFT="40">
<edge COLOR="#3333ff"/>
<cloud COLOR="#f0f0f0" WIDTH="0"/>
<node TEXT="De l&apos;allure" ID="ID_536147522" CREATED="1384264331377" MODIFIED="1384265492487" HGAP="-170" VSHIFT="-20">
<icon BUILTIN="75%"/>
</node>
<node TEXT="Des courbes en forme" ID="ID_1524296346" CREATED="1384264331377" MODIFIED="1415624364918" HGAP="-200" VSHIFT="10">
<arrowlink COLOR="#000000" DESTINATION="ID_828860948" STARTINCLINATION="-130;28;" ENDINCLINATION="226;15;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<icon BUILTIN="75%"/>
</node>
<node TEXT="jeu de forme" ID="ID_947016718" CREATED="1384264331379" MODIFIED="1384265612029" HGAP="-150" VSHIFT="20">
<richcontent TYPE="NOTE">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Peut-&#xea;tre &#xe0; diviser en deux fiches?
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="75%"/>
</node>
<node TEXT="Les tailles" ID="ID_766327644" CREATED="1384264318210" MODIFIED="1415624522068" HGAP="-171" VSHIFT="38">
<icon BUILTIN="75%"/>
</node>
<node TEXT="Mickey" ID="ID_1869809573" CREATED="1415625088925" MODIFIED="1415625092049"/>
</node>
</node>
<node TEXT="simuler et interpr&#xe9;ter une exp&#xe9;rience al&#xe9;atoire&#xa0;;" POSITION="right" ID="ID_620723806" CREATED="1384263764410" MODIFIED="1384264885587" VSHIFT="-40">
<edge COLOR="#7c0000"/>
</node>
<node TEXT="&#xe9;valuer ses chances, un gain ou une perte dans une situation relevant du hasard (calcul des probabilit&#xe9;s, variables al&#xe9;atoires discr&#xe8;tes et esp&#xe9;rance math&#xe9;matique)&#xa0;;" POSITION="right" ID="ID_422275997" CREATED="1384263764419" MODIFIED="1384265526783" VSHIFT="-30">
<edge COLOR="#00007c"/>
</node>
<node TEXT="mobiliser ses connaissances dans quelques situations issues des math&#xe9;matiques financi&#xe8;res ou des sciences &#xe9;conomiques&#xa0;;" POSITION="right" ID="ID_1976250909" CREATED="1384263764422" MODIFIED="1384265529644" VSHIFT="-40">
<edge COLOR="#007c00"/>
<arrowlink COLOR="#000000" DESTINATION="ID_766327644" STARTINCLINATION="246;12;" ENDINCLINATION="-9;21;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="utiliser le calcul vectoriel avanc&#xe9; dans le plan (produit scalaire et calcul barycentrique)&#xa0;;" POSITION="right" ID="ID_1637689388" CREATED="1384263764430" MODIFIED="1384264891421" VSHIFT="-40">
<edge COLOR="#7c007c"/>
</node>
<node TEXT="utiliser les outils de la g&#xe9;om&#xe9;trie analytique dans le plan pour l&apos;&#xe9;tude de configurations complexes (droites et cercles dans un rep&#xe8;re orthonorm&#xe9;)&#xa0;;" POSITION="right" ID="ID_707056342" CREATED="1384263764438" MODIFIED="1384264892983" VSHIFT="-40">
<edge COLOR="#007c7c"/>
</node>
<node TEXT="d&#xe9;crire et &#xe9;tudier un ph&#xe9;nom&#xe8;ne p&#xe9;riodique &#xe0; l&apos;aide des fonctions trigonom&#xe9;triques&#xa0;;" POSITION="right" ID="ID_252647175" CREATED="1384263764443" MODIFIED="1384264894873" VSHIFT="-30">
<edge COLOR="#7c7c00"/>
</node>
<node TEXT="mobiliser ses connaissances dans quelques situations issues des sciences physiques." POSITION="right" ID="ID_646884962" CREATED="1384263764454" MODIFIED="1384265390785">
<edge COLOR="#ff0000"/>
</node>
</node>
</map>
