<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Math 9" ID="ID_57977053" CREATED="1383645972439" MODIFIED="1383668801357"><hook NAME="MapStyle" zoom="0.75">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<font SIZE="20"/>
<node TEXT="Reconna&#xee;tre et utiliser les mod&#xe8;les lin&#xe9;aire et affine pour r&#xe9;soudre un probl&#xe8;me" POSITION="right" ID="ID_913730071" CREATED="1383646229217" MODIFIED="1389647659614" STYLE="bubble" HGAP="60" VSHIFT="-38">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1567667142" STARTINCLINATION="791;905;" ENDINCLINATION="-1210;-142;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Reconna&#xee;tre et utiliser le mod&#xe8;le quadratique pour r&#xe9;soudre un probl&#xe8;me" POSITION="right" ID="ID_1526665799" CREATED="1383646268538" MODIFIED="1389644847504" STYLE="bubble" HGAP="62" VSHIFT="-37">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_753279971" STARTINCLINATION="698;-36;" ENDINCLINATION="-228;37;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_706599295" STARTINCLINATION="467;0;" ENDINCLINATION="-168;-3;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_621740091" STARTINCLINATION="293;-15;" ENDINCLINATION="-89;-3;" STARTARROW="NONE" ENDARROW="NONE"/>
</node>
<node TEXT="&#xa;Reconna&#xee;tre et utiliser le mod&#xe8;le exponentiel discret pour r&#xe9;soudre un probl&#xe8;me d&apos;&#xe9;volution&#xa0; (calculs d&apos;int&#xe9;r&#xea;ts et taux de cr&#xe9;dits)" POSITION="right" ID="ID_193225021" CREATED="1383646291255" MODIFIED="1383860979333" STYLE="bubble" HGAP="63" VSHIFT="-78"/>
<node TEXT="&#xa0;&#xa;R&#xe9;soudre un probl&#xe8;me d&apos;optimisation &#xe0; l&apos;aide de l&apos;&#xe9;tude d&apos;une fonction" POSITION="right" ID="ID_1211973514" CREATED="1383646318248" MODIFIED="1389647264626" STYLE="bubble" HGAP="86" VSHIFT="-132">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1532321990" STARTINCLINATION="96;64;" ENDINCLINATION="-242;77;" STARTARROW="NONE" ENDARROW="NONE"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1630066100" STARTINCLINATION="487;0;" ENDINCLINATION="-220;31;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Fiches" POSITION="right" ID="ID_1591181699" CREATED="1383668051191" MODIFIED="1383772979350" HGAP="1784" VSHIFT="-300">
<icon BUILTIN="edit"/>
<hook NAME="FreeNode"/>
<edge STYLE="hide_edge"/>
<node TEXT="EC1" ID="ID_1370890219" CREATED="1383668169490" MODIFIED="1389646880406" HGAP="-320">
<font SIZE="14"/>
<edge WIDTH="thin"/>
<hook NAME="FreeNode"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="Le ruban adh&#xe9;sif" ID="ID_621740091" CREATED="1383668217538" MODIFIED="1383772711675" HGAP="-200" VSHIFT="20">
<icon BUILTIN="75%"/>
</node>
<node TEXT="Les combles" ID="ID_1209968031" CREATED="1383859983350" MODIFIED="1383860121104" HGAP="-200" VSHIFT="10">
<icon BUILTIN="75%"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_913730071" STARTINCLINATION="-91;17;" ENDINCLINATION="520;-22;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Le viaduc" ID="ID_753279971" CREATED="1383668225602" MODIFIED="1383772715314" HGAP="-200">
<icon BUILTIN="75%"/>
</node>
<node TEXT="Le pont" ID="ID_1651229353" CREATED="1383668233162" MODIFIED="1383860189981" HGAP="-200" VSHIFT="-10">
<icon BUILTIN="75%"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1526665799" STARTINCLINATION="-309;-109;" ENDINCLINATION="608;124;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Les logements" ID="ID_1390446850" CREATED="1383668238290" MODIFIED="1383772720448" HGAP="-200" VSHIFT="-10">
<icon BUILTIN="75%"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_33954028" STARTINCLINATION="-181;0;" ENDINCLINATION="258;0;" STARTARROW="NONE" ENDARROW="NONE"/>
</node>
<node TEXT="Les lunules" ID="ID_97644792" CREATED="1383668245381" MODIFIED="1389646822036" HGAP="-200" VSHIFT="-20">
<icon BUILTIN="50%"/>
</node>
<node TEXT="Le vide poche" ID="ID_1532321990" CREATED="1383681830950" MODIFIED="1384895049821" HGAP="-200" VSHIFT="-20">
<icon BUILTIN="75%"/>
</node>
<node TEXT="Tyne Bridge" ID="ID_706599295" CREATED="1383772610076" MODIFIED="1389646873679" HGAP="-200" VSHIFT="20">
<icon BUILTIN="75%"/>
</node>
<node TEXT="Florence" ID="ID_1626839104" CREATED="1383861781072" MODIFIED="1387234922996" HGAP="-200" VSHIFT="10">
<icon BUILTIN="25%"/>
</node>
<node TEXT="Sortie nocturne" ID="ID_1720171160" CREATED="1383861789833" MODIFIED="1389651777980" HGAP="-200" VSHIFT="20">
<icon BUILTIN="25%"/>
</node>
</node>
<node TEXT="EC2" ID="ID_1377209711" CREATED="1383668201497" MODIFIED="1389646885988" HGAP="-310" VSHIFT="360">
<font SIZE="12"/>
<hook NAME="FreeNode"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
<node TEXT="Probl&#xe8;mes du second degr&#xe9;" ID="ID_617755850" CREATED="1383668642765" MODIFIED="1384265288157" HGAP="-210" VSHIFT="-10">
<icon BUILTIN="50%"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1526665799" STARTINCLINATION="-141;-3;" ENDINCLINATION="544;16;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Le prix du gaz" ID="ID_1715321599" CREATED="1383861006964" MODIFIED="1389647171435" COLOR="#0000cc" HGAP="-210" VSHIFT="10">
<icon BUILTIN="50%"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_193225021" STARTINCLINATION="-218;107;" ENDINCLINATION="-102;171;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="La brique de lait" ID="ID_1630066100" CREATED="1383861054984" MODIFIED="1389647268587" COLOR="#0000cc" HGAP="-236" VSHIFT="33">
<icon BUILTIN="0%"/>
</node>
<node TEXT="Co&#xfb;t marginal" ID="ID_1134494534" CREATED="1383861700477" MODIFIED="1389647625317" HGAP="-210" VSHIFT="10">
<icon BUILTIN="50%"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1211973514" STARTINCLINATION="-318;-7;" ENDINCLINATION="724;197;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="" ID="ID_235402271" CREATED="1383861722299" MODIFIED="1383861729297" HGAP="-210" VSHIFT="10">
<icon BUILTIN="0%"/>
</node>
<node TEXT="" ID="ID_1990865390" CREATED="1383861735386" MODIFIED="1384895067104" HGAP="-210" VSHIFT="30">
<icon BUILTIN="0%"/>
</node>
<node TEXT="Mise en &#xe9;quation" ID="ID_1503391146" CREATED="1383668696138" MODIFIED="1384895070753" HGAP="-210" VSHIFT="50">
<icon BUILTIN="50%"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_913730071" STARTINCLINATION="-101;0;" ENDINCLINATION="377;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Syst&#xe8;me lin&#xe9;aire" ID="ID_1567667142" CREATED="1383689182048" MODIFIED="1383861263145" HGAP="-210" VSHIFT="-20">
<icon BUILTIN="75%"/>
</node>
<node TEXT="Retour au port" ID="ID_1743193149" CREATED="1383668718663" MODIFIED="1389645391335" HGAP="-210" VSHIFT="-10">
<icon BUILTIN="50%"/>
</node>
<node TEXT="Franc carreau" ID="ID_1274795615" CREATED="1383681974280" MODIFIED="1389647634857" HGAP="-210">
<icon BUILTIN="25%"/>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1779595673" STARTINCLINATION="-144;0;" ENDINCLINATION="513;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
</node>
</node>
<node TEXT="utiliser la m&#xe9;thode des moindres carr&#xe9;s pour mod&#xe9;liser une s&#xe9;rie statistique" POSITION="right" ID="ID_259676566" CREATED="1383646341131" MODIFIED="1383860841039" BACKGROUND_COLOR="#fad58d" HGAP="60" VSHIFT="-120"/>
<node TEXT="D&#xe9;crire un ensemble de donn&#xe9;es chiffr&#xe9;es &#xe0; l&apos;aide de graphiques et/ou d&apos;indicateurs statistiques (moyenne, m&#xe9;diane, &#xe9;cart-type&#xa0;, etc.)" POSITION="right" ID="ID_33954028" CREATED="1383646364757" MODIFIED="1383860843140" BACKGROUND_COLOR="#fad58d" HGAP="60" VSHIFT="-83"/>
<node TEXT="&#xa;Utiliser les m&#xe9;thodes graphiques pour r&#xe9;soudre un programme lin&#xe9;aire issu d&apos;une  situation &#xe9;conomique" POSITION="right" ID="ID_1667698609" CREATED="1383646380411" MODIFIED="1383860864199" HGAP="73" VSHIFT="-16"/>
<node TEXT="&#xa0;Se rep&#xe9;rer dans le plan&#xa0;&#xe0; l&apos;aide des coordonn&#xe9;es cart&#xe9;siennes et des coordonn&#xe9;es polaires" POSITION="right" ID="ID_1201414319" CREATED="1383646395097" MODIFIED="1383860866388" BACKGROUND_COLOR="#abf793" HGAP="46" VSHIFT="53">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1743193149" STARTINCLINATION="298;0;" ENDINCLINATION="-113;-6;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="&#xa0;&#xc9;tudier des configurations g&#xe9;om&#xe9;triques planes faisant intervenir les th&#xe9;or&#xe8;mes classiques &#xe9;l&#xe9;mentaires (th&#xe9;or&#xe8;me de Thal&#xe8;s, th&#xe9;or&#xe8;me de Pythagore et trigonom&#xe9;trie dans le triangle rectangle)" POSITION="right" ID="ID_1183411689" CREATED="1383646415471" MODIFIED="1389646889001" BACKGROUND_COLOR="#abf793" VSHIFT="93">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_97644792" STARTINCLINATION="266;0;" ENDINCLINATION="-76;-19;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="Utiliser le calcul vectoriel&#xa0;" POSITION="right" ID="ID_965284672" CREATED="1383646435858" MODIFIED="1389647628767" BACKGROUND_COLOR="#abf793" VSHIFT="93">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1743193149" STARTINCLINATION="840;20;" ENDINCLINATION="-310;72;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="&#xc9;valuer ses chances dans une situation simple relevant du hasard" POSITION="right" ID="ID_1779595673" CREATED="1383646452218" MODIFIED="1383860853658" BACKGROUND_COLOR="#fad58d" HGAP="8" VSHIFT="66"/>
<node TEXT="&#xa0;Mobiliser ses connaissance dans quelques situations relevant de la mod&#xe9;lisation g&#xe9;om&#xe9;trique de probl&#xe8;mes issus des sciences physiques." POSITION="right" ID="ID_316101991" CREATED="1383646465616" MODIFIED="1383860851031" BACKGROUND_COLOR="#abf793" HGAP="8" VSHIFT="53"/>
</node>
</map>
