#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import os.path
import os
import string
import re
import random
from pyPdf import PdfFileReader
from string import Template

import sys

title = re.compile(".*TITRE.*")
water = re.compile(".*atermark.*")
fjson = re.compile("fichier=(.*\.json)")
ladate = re.compile(".*ladate{")

length = 13
chars = string.ascii_letters + string.digits + "!@#$%^*"
random.seed = os.urandom(1024)

pdfkeywords = {"/Title", "/Keywords", "/Author", "/Producer"}
t = Template("InfoBegin\nInfoKey: Producer\nInfoValue: $Producer\nInfoBegin\nInfoKey: Title\nInfoValue: $Title\nInfoBegin\nInfoKey: Keywords\nInfoValue: $Keywords\nInfoBegin\nInfoKey: Author\nInfoValue: $Author\n".encode('utf-8'))


base = "~/Documents/git/Daeu/"
# from pprint import pprint


def ecriredatatxt(template, valeurs):
    """

    :param template: param valeurs:
    :param valeurs: 

    """

    f = open("data.txt", "w")
    f.write(template.substitute(valeurs).encode('utf-8'))
    f.close()


def get_meta(fichier):
    """Récupère les métadonnées

    :param fichier: 

    """
    pdffile = open(fichier, "rb")
    s = {"Title": "", "Keywords": "", "Author": "", "Producer": ""}

    try:
        pdf_toread = PdfFileReader(pdffile)
        pdf_info = pdf_toread.getDocumentInfo()
        for mot in pdfkeywords:
            s[mot[1:]] = pdf_info[mot]
    except IOError:
        pass
    pdffile.close()
    return s


def is_float(num):
    """

    :param num: 

    """
    s = 1
    try:
        i = float(num)
    except ValueError, TypeError:
        s = 0

    return s


def create_dir(directory):
    """

    :param directory: 

    """
    os.system("mkdir -p " + directory + "/fichier_travail")
    os.system("mkdir -p " + directory + "/impression/Memo")
    os.system("mkdir -p " + directory + "/impression/EC1/guides")
    os.system("mkdir -p " + directory + "/impression/EC2/guides")
    os.system("mkdir -p " + directory + "/impression/autres/guides")
    os.system("mkdir -p " + directory + "/impression/autres/guides")
    os.system("mkdir -p " + directory + "/web/EC2/guides")
    os.system("mkdir -p " + directory + "/web/Memo")
    os.system("mkdir -p " + directory + "/web/EC1/guides")
    os.system("mkdir -p " + directory + "/web/autres/guides")
    os.system("mkdir -p " + directory + "/FMI/EC1")
    os.system("mkdir -p " + directory + "/FMI/EC2")
    os.system("mkdir -p " + directory + "/FMI/Memo")
    os.system("mkdir -p " + directory + "/Org")


def fichier_de_travail(chapitre):
    """Création fichers de travail

    :param chapitre: 

    """

    for fichier in chapitre["pdfs"]:
        SOURCE = chapitre["rep"] + "/" + fichier
        DEST = export_dir + "/fichier_travail/" + fichier
        os.system("pdftk " + SOURCE + " stamp tampon.pdf output " + DEST)
        SOURCE = string.replace(SOURCE, ".pdf", "_guide.pdf")
        if os.path.exists(SOURCE):
            DEST = string.replace(DEST, ".pdf", "_guide.pdf")
            os.system("pdftk " + SOURCE + " stamp tampon.pdf output " + DEST)

    os.system(
        "pdftk "
        + export_dir
        + "/fichier_travail/*.pdf cat output "
        + export_dir
        + "/"
        + nmodule
        + ".pdf"
    )


def test(chapitre):
    """

    :param chapitre: 

    """
    if os.path.exists(base + chapitre["rep"]):
        print chapitre["titre"] + " ............... OK"
        if chapitre.has_key("annexes"):
            print "...................... avec annexe"
    else:
        print chapitre["titre"] + " ............... fail"


def temps(chapitre):
    """renvoie le temps passé sur le chapitre

    :param chapitre: 

    """
    return int(chapitre["temps"])


def ppdf(source, destination, selection=""):
    """utilise pdftk pour exporter et <<protéger>> le fichier pdf.

    :param source: param destination:
    :param selection: Default value = "")
    :param destination: 

    """

    meta = get_meta(source)
    ecriredatatxt(t, meta)
    apass = "".join(random.choice(chars) for i in range(length))

    os.system("pdftk %s %s output %s" % (source, selection, "tmp.pdf"))
    os.system(
        "pdftk %s update_info_utf8 data.txt  output %s owner_pw %s allow printing"
        % ("tmp.pdf", destination, apass)
    )
    os.system("rm -f data.txt && rm -f tmp.pdf")


def rmwatermak(source, dest):
    """créé un fichier pdf sans watermark

    :param source: param dest:
    :param dest: 

    """
    src_tex = open(source, "r")
    dest_tex = open(dest, "w")
    while 1:
        ligne = src_tex.readline()
        if not ligne:
            break
        if not water.match(ligne):
            dest_tex.write(ligne)
    src_tex.close()
    dest_tex.close()


def setversion(source, dest, num):
    """change la version d'un document

    :param source: param dest:
    :param num: param dest:
    :param dest: 

    """
    c = 0
    src_tex = open(source, "r")
    dest_tex = open(dest, "w")
    while 1:
        ligne = src_tex.readline()
        if not ligne:
            break
        if not ladate.match(ligne):
            dest_tex.write(ligne)

        else:
            dest_tex.write("\\ladate{" + num + "}\n")
            c = c + 1
    src_tex.close()
    dest_tex.close()
    if c != 1:
        print ("Une erreur c'est produite")
        print (dest, c)


def setversions(chapitre, num):
    """

    :param chapitre: param num:
    :param num: 

    """

    for fichier in chapitre["pdfs"]:
        SOURCE = string.replace(fichier, "pdf", "tex")
        os.system("cd " + chapitre["rep"] + " && mv " + SOURCE + " tmp" + SOURCE)
        setversion(
            chapitre["rep"] + "/tmp" + SOURCE, chapitre["rep"] + "/" + SOURCE, num
        )
        os.system("cd " + chapitre["rep"] + " && rm -f tmp" + SOURCE)


def chaine_selection(npage, inverse=0):
    """renvoie une chaine pour la sélection des pages pour pdftk

    :param npage: param inverse:  (Default value = 0)
    :param inverse: Default value = 0)

    """

    selection = " cat "
    if inverse == 0:
        for i in range(npage):
            selection += str(i + 1) + " "
    else:
        selection += str(npage + 1) + "-end "
    return selection


def impression(chapitre):
    """Création fichers destinés à l'impression

    :param chapitre: 

    """
    for fichier in chapitre["pdfs"]:
        SOURCE = string.replace(fichier, "pdf", "tex")
        rmwatermak(chapitre["rep"] + "/" + SOURCE, chapitre["rep"] + "/tmp" + SOURCE)
        os.system(
            "cd " + chapitre["rep"] + " && rubber --unsafe -m pdftex  tmp" + SOURCE
        )

        ## sélection des pages de l'énoncé et copie dans le répertoire "impression"
        selection = chaine_selection(int(chapitre["npages"]))
        SOURCE = chapitre["rep"] + "/tmp" + fichier
        DEST = export_dir + "/impression/" + chapitre["dest"] + "/" + fichier

        ppdf(SOURCE, DEST, selection)

        SOURCE = string.replace(fichier, ".pdf", "_guide.tex")

        if os.path.exists(chapitre["rep"] + "/" + SOURCE):
            rmwatermak(
                chapitre["rep"] + "/" + SOURCE, chapitre["rep"] + "/tmp" + SOURCE
            )
            os.system(
                "cd " + chapitre["rep"] + " && rubber --unsafe -m pdftex  tmp" + SOURCE
            )
            SOURCE = string.replace(fichier, ".pdf", "_guide.pdf")
            if os.path.exists(chapitre["rep"] + "/" + SOURCE):
                os.system(
                    "mv "
                    + chapitre["rep"]
                    + "/tmp"
                    + SOURCE
                    + " "
                    + export_dir
                    + "/impression/"
                    + chapitre["dest"]
                    + "/guides/"
                    + SOURCE
                )
        else:
            print (
                "il n'y a pas encore de guide pour la fiche: "
                + string.replace(fichier, ".pdf", "")
            )
        SOURCE = chapitre["rep"] + "/tmp*"
        os.system("rm -f " + SOURCE)


def web(chapitre):
    """Création fichiers destinés à PassPort

    :param chapitre: 

    """
    for fichier in chapitre["pdfs"]:
        source = chapitre["rep"] + "/" + fichier
        destination = export_dir + "/web/" + chapitre["dest"] + "/" + fichier
        ppdf(source, destination)
        source = string.replace(source, ".pdf", "_guide.pdf")
        if os.path.exists(source):
            destination = export_dir + "/web/" + chapitre["dest"] + "/guides/" + fichier
            destination = string.replace(destination, ".pdf", "_guide.pdf")
            ppdf(source, destination)

    if chapitre.has_key("annexes"):
        for fichier in chapitre["annexes"]:
            os.system(
                "cp "
                + chapitre["rep"]
                + "/"
                + fichier
                + " "
                + export_dir
                + "/web/"
                + chapitre["dest"]
                + "/"
            )


def illustration(illu):
    """

    :param illu: 

    """
    s = illu[0]["pic"]
    t = "{attach}images/" + illu[0]["pic"]
    return [s, t]


def traitementFichier(chapitre):
    """

    :param chapitre: 

    """
    s = ""
    fichier = chapitre["pdfs"][0]
    if chapitre["dest"] == "Memo":
        chemin = export_dir + "/FMI/" + chapitre["dest"] + "/" + fichier
    else:
        chemin = (
            export_dir
            + "/FMI/"
            + chapitre["dest"]
            + "/"
            + chapitre["rep"]
            + "/"
            + fichier
        )
    if chapitre["dest"] == "Memo":
        s += "[Memo]({attach}docs/" + fichier + ")"
        os.system("cp " + chemin + " ../pelican/content/docs/")
    else:
        s += "[Énoncé]({attach}docs/" + fichier + ")"
        os.system("cp " + chemin + " ../pelican/content/docs/")

        correction = string.replace(chemin, ".pdf", "_cor.pdf")
        if os.path.exists(correction):
            os.system("cp " + correction + " ../pelican/content/docs/")
            s += (
                " - [Éléments de correction]({attach}docs/"
                + string.replace(fichier, ".pdf", "_cor.pdf")
                + ")"
            )

        guide = string.replace(chemin, ".pdf", "_guide.pdf")
        if os.path.exists(guide):
            os.system("cp " + guide + " ../pelican/content/docs/")
            s += (
                " - [Guide de travail]({attach}docs/"
                + string.replace(fichier, ".pdf", "_guide.pdf")
                + ")"
            )

    return s


def md(chapitre):
    """Création des fichiers destinés à Pelican

    :param chapitre: 

    """
    sorg = "Title: $titre\nDate: $date\nCategory: $categorie\nTags: $tags\n# Objectifs pédagogiques:\n$peda\n# Fichiers:\n$fichiers\n"

    fs = export_dir + "/Org/" + string.replace(chapitre["pdfs"][0], ".pdf", ".md")
    fs = "../pelican/content/" + string.replace(chapitre["pdfs"][0], ".pdf", ".md")
    f = open(fs, "w")

    ## indexation
    orgval = {
        "titre": "",
        "date": "",
        "categorie": "",
        "tags": "",
        "peda": "",
        "fichiers": "",
        "annexes": "",
    }
    orgval["titre"] = chapitre["titre"]
    orgval["date"] = "2017-06-19"
    orgval["categorie"] = nmodule
    #     orgval["tags"]=nmodule+"-"+chapitre["dest"]
    if chapitre.has_key("mc"):
        orgval["tags"] = orgval["tags"] + ", " + chapitre["mc"]
    if chapitre.has_key("peda"):
        orgval["peda"] = chapitre["peda"]

    ## annexes
    if chapitre.has_key("annexes"):
        sorg = sorg + "#  Annexes:\n $annexes"
        for fichier in chapitre["annexes"]:
            os.system(
                "cp "
                + export_dir
                + "/web/"
                + chapitre["dest"]
                + "/"
                + fichier
                + " ../pelican/content/docs/"
            )
            orgval["annexes"] = (
                orgval["annexes"]
                + "* ["
                + fichier
                + "]({attach}docs/"
                + fichier
                + ")\n"
            )

    orgval["fichiers"] = traitementFichier(chapitre)

    ## Illustrations
    if chapitre.has_key("illustration"):
        os.system(
            "cp "
            + chapitre["rep"]
            + "/"
            + chapitre["illustration"][0]["pic"]
            + " ../pelican/content/images/"
        )

        param = illustration(chapitre["illustration"])
        s = "![{0}]({1})".format(*param)
        s = (
            s
            + "\n\nCrédit photo: ["
            + chapitre["illustration"][0]["credit"][0]["name"]
            + "]("
            + chapitre["illustration"][0]["credit"][0]["url"]
            + ")\n\n"
        )
        sorg = string.replace(sorg, "# Objectifs", s + "\n# Objectifs")
    sorg = sorg + "\n# Sources\n"

    sorg += (
        "["
        + chapitre["titre"]
        + "](https://framagit.org/vincek/mathdaeucueep/tree/master/"
        + chapitre["rep"]
        + "/)\n"
    )
    torg = Template(sorg)
    f.write(torg.substitute(orgval))
    f.close()


def fmi(chapitre):

    """Création fichers destinés à PassPort FMI
    à lancer après web pour avoir les guides à jour

    :param chapitre: 

    """
    for fichier in chapitre["pdfs"]:
        source = chapitre["rep"] + "/" + fichier
        if (
            (chapitre["dest"] == "EC1")
            or (chapitre["dest"] == "EC2")
            or (chapitre["dest"] == "autres")
        ):
            # print "FMI fiches:"+chapitre["titre"]
            repertoire = export_dir + "/FMI/" + chapitre["dest"] + "/" + chapitre["rep"]
            enonce = (
                export_dir
                + "/FMI/"
                + chapitre["dest"]
                + "/"
                + chapitre["rep"]
                + "/"
                + fichier
            )
            correction = string.replace(enonce, ".pdf", "_cor.pdf")
            guide = string.replace(
                export_dir + "/web/" + chapitre["dest"] + "/guides/" + fichier,
                ".pdf",
                "_guide.pdf",
            )
            nbpages = int(chapitre["npages"])
            if not os.path.exists(repertoire):
                os.system("mkdir -p " + repertoire)

            ppdf(source, enonce, chaine_selection(nbpages))
            ppdf(source, correction, chaine_selection(nbpages, 1))

            if os.path.exists(guide):
                os.system("cp " + guide + " " + repertoire + "/")

            if chapitre.has_key("annexes"):
                for fichier in chapitre["annexes"]:
                    os.system(
                        "cp " + chapitre["rep"] + "/" + fichier + " " + repertoire + "/"
                    )

        else:
            # print "FMI memo:"+chapitre["titre"]
            enonce = export_dir + "/FMI/" + chapitre["dest"] + "/" + fichier
            ppdf(source, enonce)


def build(chapitre):
    """Compilation

    :param chapitre: 

    """

    for fichier in chapitre["pdfs"]:
        SOURCE = string.replace(fichier, "pdf", "tex")
        os.system("cd " + chapitre["rep"] + " && rubber --unsafe  -m pdftex " + SOURCE)
        SOURCE = string.replace(fichier, ".pdf", "_guide.tex")

        if os.path.exists(chapitre["rep"] + "/" + SOURCE):
            os.system(
                "cd " + chapitre["rep"] + " && rubber --unsafe -m pdftex " + SOURCE
            )


def nettoyage():
    """Nettoyage des fichiers temporaires"""

    print "Nettoyage"
    motifs = ['"*~"', '"*.bak"', '"*.log"', '"*.aux"', "ans1.tex", '"*.out"']
    for motif in motifs:
        os.system("find . -name " + motif + " -exec rm -f {} \;")


def baide(chapitre):
    """construits les fichiers guides à partir de patron.tex

    :param chapitre: 

    """

    for fichier in chapitre["pdfs"]:
        source = open("patron.tex", "r")
        SOURCE = string.replace(fichier, ".pdf", "_guide.tex")
        if not os.path.exists(chapitre["rep"] + "/" + SOURCE):
            dest = open(chapitre["rep"] + "/" + SOURCE, "w")
            titre = chapitre["titre"]

            while 1:
                ligne = source.readline()
                if not ligne:
                    break
                if title.match(ligne):
                    ligne = string.replace(ligne, "TITRE", titre)
                dest.write(ligne)


def doit(chapitre):
    """Étapes de création

    :param chapitre: 

    """
    test(chapitre)

    ################	Compilation #####################
    build(chapitre)

    ################	Création fichers de travail ####################
    fichier_de_travail(chapitre)

    ################	Création fichers destinés à l'impression ####################
    impression(chapitre)

    ################	Création fichers destinés à PassPort ####################
    web(chapitre)
    ################	Création fichers destinés à PassPort ####################
    fmi(chapitre)


def usage():
    """ """
    print (
        "Usage: export.py fichier=FICHIER.json\n Options disponibles:\n temps: Détermine le temps de réalisation \n build_aide:Créé les fichiers guide\n createdir:\n version num : place les fichier en version num\n"
    )


if len(sys.argv) == 1:
    usage()
    sys.exit(1)

if len(sys.argv) > 1:
    args = sys.argv[1:]
    fichier_desc_module = ""
    for arg in args:
        if fjson.match(arg):
            fichier_desc_module = fjson.match(arg).group(1)
            args.remove(arg)
    if fichier_desc_module == "":
        usage()
        sys.exit(1)

    ### initialisation #######
    os.system("rubber --unsafe  -m pdftex  tampon.tex > /dev/null")
    json_data = open(fichier_desc_module)
    data = json.load(json_data)
    json_data.close()
    nmodule = data["module"]
    export_dir = "export" + nmodule
    ##### org ##########
    try:
        args.index("md")
    except ValueError:
        "Do nothing"
    else:

        for chapitre in data["fichiers"]:
            md(chapitre)
        print "création fichiers org "
        os.system("cd ../pelican && pelican content")
        sys.exit(1)
    ##### temps ##########
    try:
        args.index("temps")
    except ValueError:
        "Do nothing"
    else:
        t = 0
        for chapitre in data["fichiers"]:
            t += temps(chapitre)
        print "temps de création: " + str(t) + " heures"
        sys.exit(1)
    ##### guide_aide #########
    try:
        args.index("build_aide")
    except ValueError:
        "Do nothing"
    else:
        print ("Création des fichiers guides manquants à partir de patron.tex")
        for chapitre in data["fichiers"]:
            baide(chapitre)
        sys.exit(1)
    ###### création des répertoires ######
    try:
        args.index("createdir")
    except ValueError:
        "Do nothing"
    else:
        print ("Création des répertoires")
        create_dir(export_dir)
        sys.exit(1)

    ###### version ##################
    try:
        args.index("version")
    except ValueError:
        "Do nothing"
    else:

        if len(args) == 2 and is_float(args[1]):
            print ("La version des fichiers sera:" + args[1])
            for chapitre in data["fichiers"]:
                setversions(chapitre, args[1])
        elif len(args) == 3 and is_float(args[2]):
            print ("La version du fichier " + args[1] + " sera:" + args[2])
            for chapitre in data["fichiers"]:
                try:
                    args.index(chapitre["rep"])

                except ValueError:
                    "Do nothing"
                else:
                    setversions(chapitre, args[2])
        else:
            usage()
        sys.exit(1)

    if len(args) == 0:
        for chapitre in data["fichiers"]:
            doit(chapitre)

    else:
        for chapitre in data["fichiers"]:
            try:
                args.index(chapitre["rep"])

            except ValueError:
                "Do nothing"
            else:
                doit(chapitre)


nettoyage()
