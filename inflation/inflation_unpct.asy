// préambule asymptote
usepackage("amsmath,amssymb");
usepackage("inputenc","utf8");
// code figure
import graph;
settings.render = 8;
unitsize(0.1cm,1cm);
xlimits(-10,250);
ylimits(-1,12);
xaxis(BottomTop,xmin=-10,xmax=250,Ticks("%",extend=true,Step=1),p=linewidth(0.5pt)+gray);
yaxis(LeftRight,ymin=-1,ymax=12,Ticks("%",extend=true,Step=0.1),p=linewidth(0.5pt)+gray);

xaxis(BottomTop,xmin=-10,xmax=250,Ticks("",extend=true,Step=10),p=linewidth(0.7pt)+gray);
yaxis(LeftRight,ymin=-1,ymax=12,Ticks("",extend=true,Step=1),p=linewidth(0.7pt)+gray);

real F(real x) {return 1.01^x;}
draw(graph(F,0,250,n=400),linewidth(1.5pt)+blue+solid);
xlimits(-10,250,Crop);
ylimits(-1,13,Crop);
xaxis(axis=YEquals(0),xmin=0,xmax=250,Ticks(scale(.7)*Label(),beginlabel=true,endlabel=true,begin=true,end=true,NoZero,Step=10,Size=1mm),p=linewidth(1pt)+black,true);
yaxis(axis=XEquals(0),ymin=0,ymax=12,Ticks(scale(.7)*Label(),beginlabel=true,endlabel=true,begin=true,end=true,NoZero,Step=1,Size=1mm),p=linewidth(1pt)+black,true);

label("\Large $y=1,01^x$",(120,11));

//labelx(Label("$O$",NoFill), 0, SW);
//draw(Label("$\vec{\imath}$",NoFill), (0,0)--(1,0),S,Arrow(2mm));
//draw(Label("$\vec{\jmath}$",NoFill), (0,0)--(0,1),W,Arrow(2mm));
dot((0,0));
// code supplémentaire

shipout(bbox(0.1cm,0.1cm,white));
