%%% Fichier mis à disposition selon la licence CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) %%%
\documentclass[A4,12pt]{cueep}
\renewcommand{\SFC}{\includegraphics[height=8mm]{../logo_lille1}\includegraphics[height=8mm]{../ccbyncsa}}
 
% rubber: setlist arguments --shell-escape

%%% Local Variables:
%%% mode: pdflatex
%%% coding: utf-8
%%% LaTeX-command: "pdflatex --shell-escape"
%%% End: 



\usepackage[xcas]{pro-tablor}

\titre{Pente d'une courbe\\
Nombre dérivé}


\usepackage{draftwatermark}
\SetWatermarkScale{1.7}
\SetWatermarkAngle{300}
\SetWatermarkLightness{0.95}
\SetWatermarkText{Math 9}
\usepackage[tikz]{bclogo}

\ladate{0.99}
\reference{Memo}

\motsclefs{pente, dérivée, DAEU, Math 9}




\begin{document}



\section{Le nombre dérivé}

On se donne une courbe et un point $A$ sur  cette courbe. Maintenant
soit $M$ un point distinct de $A$. 


On dit que la courbe admet une pente en $A$, si la droite $(AM)$ se
<<rapproche>> d'une droite non verticale lorsque que $M$ se rapproche
de $A$.


Le cas échéant la droite <<limite>> est appelée la tangente à la
courbe en $A$ et la pente de la tangente est la pente de la courbe en $A$.



\begin{center}
  \includegraphics[width=13cm]{nb_derive}
\end{center}




Si $A$ a pour coordonnées $(a;f(a))$, on note $f'(a)$ la pente de la
courbe en $A$ (si elle existe). $f'(a)$ est également appelé le nombre dérivé.



\section{Courbe des pentes}


Soit $\mathcal{C}$ une courbe qui admet une pente en tout point. Si 
$\mathcal{C}$ est la courbe représentative d'une fonction $f$ définie
sur un intervalle $I$, on dit que la fonction $f$ est dérivable sur $I$.


Lorsque $x$ parcourt $I$, la relation $y=f'(x)$ définit une courbe
appelée courbe des pentes ou courbe dérivée. Cette courbe donne la
pente de $\mathcal{C}$ en tout point. La fonction $f'$ ainsi définie
(à chaque valeur de $x$ on associe la pente de la courbe au point
d'abscisse $x$) est appelée fonction dérivée.



\begin{center}
  \includegraphics[width=13cm]{nb_derive_fig2}
\end{center}

\section{Linéarité de la dérivation}
\label{line}

\begin{pro}
  Si l'on se donne deux fonctions $f$ et $g$ dérivables en $a$ et
  $\lambda$ un nombre. Alors:



  \begin{Itemize}

  \item la fonction $f+g$ est dérivable en $a$ et 
    $f'(a)+g'(a)$ est le nombre dérivé de $f+g$ au point d'abscisse $a$;
  \item la $\lambda\cdot f$ est dérivable en $a$ et $\lambda f'(a)$
    est le nombre dérivé de $\lambda \cdot f$ au point d'abscisse $a$.
  \end{Itemize}
\end{pro}


\begin{exem}
  Si $f(x)=3x+1$ et $g(x)=-7x+2$ alors $f'(x)=3$ et $g'(x)=-7$. (Pour
  les fonctions affines la pente est constante et correspond à la
  pente de la droite associée).

Maintenant si on pose $h(x)=-3f(x)+g(x)$ alors $h'(x)=-3\times 3+(-7)=-16$.
\end{exem}
 


\section{Formulaire}
\label{form}

Le tableau suivant donne l'expression de la dérivée pour quelques
fonctions de références.

\renewcommand{\arraystretch}{1.5}

Ici, $\R$ désigne l'ensemble de tous les nombres, $\R$ est l'ensemble
des nombres réels. L'ensemble $\R\backslash\{0\}$ désigne tous les nombres
réels non nuls. L'ensemble $\N=\{0;1;2;3;\cdots\}$ est l'ensemble des
entiers dit naturels.

\[\begin{array}[h]{|*2{c|}|*2{c|}}
\hline
f(x)&\text{Ensemble de validité}&f'(x)&\text{Ensemble de validité}\\
\hline\hline
k&\R &0&\R \\\hline
ax+b&\R &a &\R \\\hline
x^2&\R&2x&\R\\\hline
x^3&\R&3x^2&\R\\\hline
x^n ;n\in\mathbb{N}& \R & nx^{n-1} &\R \\\hline
\frac{1}{x} &\R\backslash\{0\}& -\frac1{x^2}&\R\backslash\{0\} \\\hline

\sqrt{x} &[0;+\infty[ &\frac1{2\sqrt{x}}&]0;+\infty[\\\hline

\end{array}\]

Ce tableau  n'est pas exhaustif.


\begin{exemple}
  Quelle est la pente de la courbe d'équation $y=x^3-2x^2-6x+1$ au point
  d'abscisse 3?

L'équation de la courbe des pentes est $y=3x^2-4x-6$ donc la pente au point
  d'abscisse 3 est 9 ($3\times 3^2-4\times 3-6=9$).

\end{exemple}



\begin{exemple}
  Soit $f$ une fonction définie par $x\longmapsto 6x^2+\sqrt{x}$ sur
  $[0;+\infty[$. Une expression de la dérivée de $f$ est donnée par
  \begin{equation*}
    f'(x)=12x+\frac{1}{2\sqrt{x}}
  \end{equation*}


\begin{bclogo}[arrondi = 0.1,logo=\bcdanger]{Attention!}
 La fonction $f$ n'est pas dérivable en 0.
\end{bclogo}

\end{exemple}


\begin{exemple}
  Soit $g$ la fonction définie sur $]0;+\infty[$ par
  $g(x)=x-\frac{1}{x}$. Que vaut le nombre dérivée en 2?


Tout d'abord,  $g'(x)=1-\frac{-1}{x^2}=1+\frac{1}{x^2}$ et $g'(2)=\frac{5}{4}$.
\end{exemple}
\section{Équation de la tangente}
\label{sec:tan}

Si $M(x;y)$  appartient à $T_A$ la tangente à la courbe $\mathcal{C}$
au point $A$, on a deux expressions pour la pente de cette droite
$f'(a)$ et $\frac{y-f(a)}{x-a}$, on en déduit que 




\begin{center}
  \includegraphics[width=12cm]{nb_derive_fig3}
\end{center}



\begin{equation*}
  \frac{y-f(a)}{x-a}=f'(a)
\end{equation*}
\begin{pro} Soit $f$ une fonction dérivable en $a$, on nomme $T_A$
  l'équation de la tangente à $\mathcal{C}_f$ au point d'abscisse $a$.
 L'équation de la tangente $T_A$:
\begin{equation*}
  y=f'(a)(x-a)+f(a)
\end{equation*}
\end{pro}

\begin{exemple}
  Posons $f(x)=x^3-x+1$, $f'(x)=3x^2-1$. Au point $A(1;1)$ la pente
  est $f'(1)=3\times1^2-1=2$. Et l'équation de la tangente à la courbe
  d'équation $y=f(x)$ est: $y=2(x-1)+1=2x-1$.
\end{exemple}


\section{Calcul des dérivées}
\label{sec:calcul-des-derivees}

Les parties \ref{form} et \ref{line} permettent de déterminer
l'expression de la dérivée de certaines fonctions. Ce qui suit permet
d'aller plus loin.


\subsection{Dérivée d'un produit}
\label{sec:derivee-dun-produit}

\begin{pro}
  Soit $f$ et $g$ deux fonctions dérivable en $a$. La fonction produit
  est dérivable en $a$ et

  \begin{equation*}
    [f\times g]'(a)=f'(a)g(a)+f(a)g'(a)
  \end{equation*}
\end{pro}

\begin{exem} 


  Quelle est l'expression de la dérivée de $h(x)=x^2\sqrt{x}$ ($x>0$)?

  Si  $\begin{array}[h]{c}
    f(x)=x^2\\g(x)=\sqrt{x}
  \end{array}$  alors  $\begin{array}[h]{c}
    f'(x)=2x\\
g'(x)=\frac{1}{2\sqrt{x}}\\
   \end{array}$
 et 
$h'(x)=2x\sqrt{x}+\frac{x^2}{2\sqrt{x}}$.
\end{exem}


\subsection{Dérivée d'un quotient}
\label{sec:derivee-dun-quotient}

\begin{pro}
   Soit $f$ et $g$ deux fonctions dérivable en $a$ telle que $g(a)\neq
   0$ . La fonction quotient
  est dérivable en $a$ et

  \begin{equation*}
    \left[\frac{f}{g}\right]'(a)=\frac{f'(a)g(a)-f(a)g'(a)}{(g(a))^2}
  \end{equation*}
\end{pro}


\begin{exem}
  

  Quelle est l'expression de la dérivée de $h(x)=\frac{4x+1}{2x-1}$
  ($x\neq 0,5$)?

  Si  $\begin{array}[h]{c}
    f(x)=4x+1\\g(x)=2x-1
  \end{array}$  alors  $\begin{array}[h]{c}
    f'(x)=4\\
g'(x)=2
   \end{array}$
 et 
$h'(x)=\frac{4(2x-1)-2(4x+1)}{(2x-1)^2}=\frac{-6}{(2x-1)^2}$.
\end{exem}

\subsection{Dérivée d'une composée}
\label{sec:derivee-dun-quotient}

\begin{pro}
   Soit $f$ une fonction  dérivable en $a$  et $g$ une fonction
   dérivable en $f(a)$.

La fonction composée de $f$ par $g$ est dérivable en $a$ et le nombre
dérivée d'une composée est le produit des nombres dérivés plus
précisément:

 

  \begin{equation*}
    \left[g\circ f\right]'(a)=f'(a)\times g'(f(a))
  \end{equation*}
\end{pro}


\begin{exem}
  Quelle est la dérivée de $k(x)=\left(\frac{4x+1}{2x-1}\right)^3$
  ($x\neq 0,5$) ?



  Si  $\begin{array}[h]{c}
    h(x)=\frac{4x+1}{2x-1}\\l(x)=x^3
  \end{array}$  alors  $\begin{array}[h]{c}
    h'(x)=\frac{-6}{(2x-1)^2}\\
l'(x)=3x^2
   \end{array}$.




On peut représenter la situation par un schéma:

   \[\begin{array}[h]{ccccc}
     x&\longmapsto& \frac{4x+1}{2x-1}&&\\
     &|&X&\longmapsto&X^3\\
     &\downarrow&&\downarrow&\\
     &\frac{-6}{(2x-1)^2}&\times&3X^2&
   \end{array}\]

Ainsi $k'(x)=\frac{-6}{(2x-1)^2}\times 3\left(\frac{4x+1}{2x-1}\right)^2=\frac{-18}{(2x-1)^2}\times \left(\frac{4x+1}{2x-1}\right)^2$
\end{exem}

\section{Sens de variation des fonctions dérivables}
\label{sec:sens-de-variation}


\begin{thm}[Admis]
  Soit $f$ une fonction dérivable sur un intervalle $I$.

  \begin{Itemize}

  \item $f$ est croissante sur $I$ si et seulement si $f'(x)\geq 0$
    sur $I$;
  \item $f$ est décroissante sur $I$ si et seulement si $f'(x)\leq 0$
    sur $I$;
  \item $f$ est constante sur $I$ si et seulement si $f'(x)= 0$
    sur $I$.
  \end{Itemize}
\end{thm}


On peut résumer approximativement le théorème précédent par: <<Le signe de $f'(x)$ donne
les variations de $f$>>.













\begin{figure}[h!]
  \centering
    \includegraphics[width=12cm]{nb_derive_fig4}
  \caption{Lorsque $f'(x)$ est positif $f$ est croissante, lorsque $f'(x)$ est négatif $f$ est décroissante.}
  \label{fig:variation}
\end{figure}



\pagebreak

Pour déterminer le sens de variation d'une
fonction $f$ dérivable sur $I$, on peut procéder de la manière suivante:







\begin{enumerate}[1)]
\item On calcule $f'(x)$
\item On cherche une éventuelle expression factorisée de $f'(x)$
\item On étudie le signe de $f'(x)$
\item On dresse le tableau de variation de $f$ à partir du signe de $f'(x)$
\end{enumerate}


\begin{exemple}
  Pour étudier les variations de $V:x\longmapsto 4x^3-120x^2+900x$ sur
  l'intervalle $[0;15]$, on
  étudie  le signe de $V'(x)=12x^2-240x+900$ et l'on dresse le tableau
  de variation suivant:
\begin{TV}
TV([0,15],[],"V","x",4*x^3-120*x^2+900*x,1,n,\tv)
\end{TV}
\end{exemple}

\section{En vidéo...}
\label{sec:en-video}


\begin{bclogo}[arrondi=0.1,logo=\bcoeil]{Animation}
  Illustrations en video sur
  \href{pod.univ-lille1.f}{lille1.\up{pod}} à l'aide de \href{https://www.geogebra.org/}{Geogebra}:
  \begin{itemize}
  \item
    \href{http://pod.univ-lille1.fr/video/1759-le-nombre-derive/}{Le
      nombre dérivé};
  \item \href{http://pod.univ-lille1.fr/video/1758-la-courbe-des-pentes/}{La
    courbe des pentes}.
  \end{itemize}
\end{bclogo}



\end{document}
