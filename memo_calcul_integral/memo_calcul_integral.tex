%%% Fichier mis à disposition selon la licence CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) %%%
\documentclass[A4,12pt]{cueep}
\renewcommand{\SFC}{\includegraphics[height=8mm]{../logo_lille1}\includegraphics[height=8mm]{../ccbyncsa}}
 
\titre{Le calcul intégral}



\usepackage{array}
\usepackage{draftwatermark}
\SetWatermarkScale{1.7}
\SetWatermarkAngle{300}
\SetWatermarkLightness{0.95}
\SetWatermarkText{Math 10}
\reference{Memo}

\usepackage[tikz]{bclogo}
\ladate{0.85}

\motsclefs{calcul intégral, primitive, DAEU, Math 10}

\begin{document}

\section{Notion d'intégrale}
\label{sec:notion-dintegrale}

\subsection{Définition de l'intégrale dans le cas d'une fonction positive}
\label{sec:defin-de-lint}


On se donne une courbe  qui représente une
fonction $f$ continue et positive. C’est à dire, une courbe
représentative d'une fonction que
l’on peut tracer d’un seul coup de crayon\footnote{Pour définir
  précisément la notion de fonction continue, on a besoin du
  formalisme des limites qui sera vu en Math 11} au dessus de l’axe des
abscisses dans un repère donné.

\begin{center}
   \includegraphics[width=12cm]{calcul_integral_fig5}
\end{center}

 L'aire du domaine délimité par l'axe des abscisses, les droites d'équation $x=a$ et $x=b$ et la courbe
d'équation $y=f(x)$ est
notée



\begin{equation*}
  \int_a^bf(x)\D{x}
\end{equation*}




\begin{figure}[h!]\begin{center}\label{fig7}
  \includegraphics[width=9cm]{flute_de_champagne_fig7}
\end{center}\end{figure}

L'aire sous la courbe s'exprime en fonction de l'aire du rectangle unité.
C'est à dire le rectangle ayant pour sommet l'origine du repère et
dont la longueur vaut une unité de l'axe des abscisses et la largeur
une unité de l'axe des ordonnées.

Par
exemple:
\begin{itemize}
\item si l'unité des axes est le centimètre alors l'aire sous la courbe sera
  exprimée en cm\up{2};
\item si l'axe des abscisses représente un temps exprimé en seconde
  et l'axe des ordonnées représente une vitesse exprimée en mètre par
  seconde alors l'aire sous la courbe sera
  exprimée en mètre.
\end{itemize}
Si les unités des
axes ne sont pas précisées, on exprimera l'aire sous la courbe en
unité d'aire \textbf{ua}.




\begin{center}
   \includegraphics[width=12cm]{flute_de_champagne_fig8}
\end{center}


\begin{remarque}
  La variable d'intégration est dite muette:
    \begin{equation*}
      V(y)=\int_0^{y}\pi\frac{x}{2}\D{x}=\int_0^{y}\pi\frac{t}{2}\dt=\int_0^{y}\pi\frac{y}{2}\dy
    \end{equation*}

Toutefois, on évitera la dernière écriture.

\end{remarque}



\subsection{Notion de primitive}
\label{sec:notion-de-primitive}

On se donne une fonction continue sur l'intervalle $[a;b]$.

Soit $x$ un réel de l'intervalle de $[a;b]$,  et $\mathcal{A}$ la
fonction qui a $x$ associe l'aire du domaine délimité par la courbe et
l'axe des abscisses entre $a$ et $x$. 


\begin{center}
  \includegraphics[width=11cm]{calcul_integral_2_fig1}
\end{center}


La fonction $\mathcal{A}$ est la primitive de $f$ qui s'annule en $a$.


\begin{thm}[admis]

La fonction $\mathcal{A}$ admet une pente en tout point
plus précisément sa pente en $x$ est $f(x)$. En d'autre terme:

\begin{equation*}
   \mathcal{A}'(x)=f(x)
\end{equation*}
\footnote{Ici l'hypothèse
  de continuité est fondamentale}
  
\end{thm}


\begin{preuve}\textbf{(Idée de la preuve...)}\\


Lorsque $h\neq 0$ est très proche de 0 alors $\mathcal{A}'(x)\simeq
\frac{1}{h}(\mathcal{A}(x+h)-\mathcal{A}(x))$ car le nombre dérivé n'est autre que le taux
d'accroissement de la fonction pour $h$ infiniment proche de 0.


Or en interprétant la différence des deux aires, on a:
$\mathcal{A}(x+h)-\mathcal{A}(x)=\int_x^{x+h}f(t)\D{t}$.


et lorsque $h$ est très petit l'intégrale est proche de l'aire d'un
rectangle de hauteur $f(x)$ et de largeur $h$.

$\int_x^{x+h}f(t)\D{t}\simeq f(x)\times h$

\begin{center}
  \includegraphics[width=11cm]{calcul_integral_2_fig2}
\end{center}


Au total on a :
\begin{equation*}
  \mathcal{A}'(x)\simeq \frac{1}{h} f(x)\times h=f(x)
\end{equation*}
  
Pour justifier correctement, on a besoin de la continuité de la
fonction $f$ et d'un passage à la limite.

\end{preuve}

On a donc:

\begin{equation*}
  \mathcal{A}'(x)=f(x)
\end{equation*}




\begin{exem}
  La fonction  définie par $x\longmapsto 3x^3-6x^2+2$ est une primitive de
  $x\longmapsto 9x^2-12x$

\end{exem}

On peut donc retrouver l'expression de $\mathcal{A}$ par lecture
inverse du tableau des dérivées.
\begin{exem}
  Étant donné $f:x\longmapsto x^3-2x+1$,
  $F:x\longmapsto\frac{1}{4}x^4-x^2+x+8$ est une primitive de $f$, car $F'(x)=f(x)$.
\end{exem}


\begin{remarque}
  Si une fonction admet une primitive alors elle en admet une
  infinité. En effet, il suffit d'ajouter une constante a une primitive
  pour en avoir une autre.
\end{remarque}

\begin{pro}
  Si deux courbes ont la même pente en tout point alors elle
  s'obtiennent l'une de l'autre par une translation suivant l'axe des
  ordonnées. Dit autrement, deux primitives d'une même fonction ne
  différent qu'une d'une constante.
\end{pro}

\begin{preuve}
On suppose que l'on a deux courbes d'équation respectives $y=F(x)$ et
$y=G(x)$ telle que $F'(x)=G'(x)=f(x)$.
  

La courbe d'équation $y=F(x)-G(x)$ a une pente nulle en tout point,
donc il s'agit d'une droite horizontale dont l'équation est du type
$y=C$. Donc $F(x)=G(x)+C$ et l'on obtient la première courbe à partir
de la seconde par une translation de vecteur $C\overrightarrow{\jmath}$.
\end{preuve}


\begin{remarque}
  On dit que $F$ et $G$ sont deux primitives de $f$.
\end{remarque}


\begin{cor}
  Soit $f$ une fonction continue sur l'intervalle $[a;b]$ et $(c;C)$
  un couple de valeurs. Il existe une unique primitive de $f$ qui prend
  la valeur $C$ lorsque $x$ vaut $c$.
\end{cor}
\begin{preuve}
  D'après ce qui précède $\mathcal{A}$ est une primitive de $f$ et si
  $k$ est une constante $\mathcal{A}+k$ est également une primitive de
  $f$. Lorsque l'on évalue en $c$, on obtient $\mathcal{A}(c)+k$ on
  souhaite trouver une valeur de $k$ telle que $\mathcal{A}(c)+k=C$. Il
  suffit de prendre $k=C-\mathcal{A}(c)$. Cette valeur est unique, donc il existe une unique primitive de $f$ qui prend
  la valeur $C$ lorsque $x$ vaut $c$.
\end{preuve}
\begin{exem}
Soit $f$ définie par $f(x)=3x^2-6x+1$, on cherche la primitive de $f$
qui vaut 4 lorsque $x$ vaut 2.

$F(x)=x^3-3x^2+x+k$ définie une primitive de
$f$. $F(2)=4\Leftrightarrow 2^3-3\times2^2+2+k=4 \Leftrightarrow k=6$.

Donc $F:x\longmapsto x^3-3x^2+x+6$ est la primitive de $f$ telle que $F(2)=4$.


  
\end{exem}

\begin{exem}
  Une fonction admet une infinité de primitive, mais une seule dont la
  courbe représentative passe par un point donné.

  Voir cette
  \href{http://pod.univ-lille1.fr/video/2378-primitives/}{vidéo} (\url{http://pod.univ-lille1.fr/video/2378-primitives/}).
\end{exem}


\begin{thm}[Théorème fondamental du calcul intégral]
\label{TFCI}
Soit $f$ une fonction continue sur l'intervalle $[a;b]$ et
  $F$ une de ses primitives.
 
  \begin{equation*}
    \int_a^bf(x) \D{x}=F(b)-F(a)
  \end{equation*}


\end{thm}


\begin{preuve}
  D'après ce qui précède $\mathcal{A}$ est la primitive de $f$ qui s'annule en
  $a$ et $\mathcal{A}(b)$ et $F(b)$ ne différent que d'une constante.

  \begin{eqnarray*}
    \mathcal{A}(b)=F(b)+C\\
    \mathcal{A}(a)=F(a)+C=0\\
  \end{eqnarray*}

Donc $C=-F(a)$ et l'on a

\begin{equation*}
  \mathcal{A}(b)=  \int_a^bf(x) \D{x}=F(b)+(-F(a))=F(b)-F(a)
\end{equation*}
\end{preuve}

\section{Conventions et  propriétés de l'intégrale}
\label{sec:quelq-propr-de}

\subsection{Premières propriétés de l'intégrale}
\label{sec:ppi}



En utilisant la commensurabilité des aires, on obtient:

\begin{pro}\textbf{Relation de {\sc Chasles}}

Soit $f$ une fonction continue sur $[a;c]$ et $b$ un élément de $[a;c]$.
\begin{equation*}
  \int_a^bf(x) \D{x} + \int_b^cf(x) \D{x} = \int_a^cf(x) \D{x}
\end{equation*}

  
\end{pro}

\begin{center}
  \includegraphics[width=10cm]{chasles}
\end{center}
\begin{pro}\textbf{Linéarité}

Soit $\lambda$ un nombre réel et $f$ et $g$, deux fonctions continues sur
l'intervalle $[a;b]$.


\begin{itemize}
\item $ \int_a^bf(x) \D{x}+  \int_a^bg(x) \D{x} = \int_a^bf(x)+g(x) \D{x}$
\item $\int_a^b\lambda\times f(x) \D{x}=\lambda\times\int_a^bf(x) \D{x}$
\end{itemize}

\end{pro}


\begin{pro}
  Si $g(x)\leq f(x)$ sur l'intervalle $[a;b]$ alors 

  \begin{equation*}
     \int_a^bg(x) \D{x}\leq  \int_a^bf(x)\D{x}
  \end{equation*}
\end{pro}



\begin{center}
  \includegraphics[width=10cm]{positivite}
\end{center}

\subsection{Extension de la notion d'intégrale}
\label{sec:extension-de-la}





On a défini précédemment l'intégrale comme l'aire sous la courbe d'une fonction
positive. Ci-dessous, on étend la définition à d'autres
situation. Dans ce qui suit, $f$ reste une fonction continue.


\subsubsection{Bornes}

Si $a>b$ alors 


\begin{equation*}
  \int_a^bf(x) \D{x}=- \int_b^af(x) \D{x}
\end{equation*}


\subsubsection{Signe de $f(x)$}


Soit $f$ une fonction continue sur l'intervalle $[a;b]$.

Si $y=f(x)\leq 0$ alors $-f(x)\geq 0$ et on se ramène au cas précédent par:




\begin{equation*}
  \int_a^bf(x) \D{x}=- \int_a^b -f(x) \D{x}
\end{equation*}



Si $f(x)$ est de signe quelconque les parties au dessus de l'axe des
abscisses sont comptées positivement, celles en dessous sont comptées
négativement.



\begin{center}
  \includegraphics[width=10cm]{calcul_integral_fig7}
\end{center}

\subsubsection{Remarques importantes}
\label{sec:remarq-import}

Le théorème  \eqref{TFCI} et les propriétés précédentes restent vraies
avec une fonction continue de signe quelconque. En particulier dans
la relation de {\sc Chasles} l'ordre des bornes n'a pas d'importance,
par exemple:

\begin{equation*}
  \int_1^{-6}\sin(x)\D{x}+\int_{-6}^{12}\sin(x)\D{x}=\int_1^{12}\sin(x)\D{x}
\end{equation*}

\section{Le calcul intégral}
\label{sec:pre}

Le théorème \eqref{TFCI} assure que l'on peut calculer l'aire sous la
courbe dès lors que l'on connaît une primitive.  Ainsi de nombreux
calculs d'aires peuvent-être menés par lecture inverse du tableau des
dérivées.




\begin{center}
\renewcommand{\arraystretch}{1.5}
  \begin{tabular}[h]{c|>{$}c<{$}|>{$}c<{$}|}
    \cline{2-3}
    &f(x)&F(x)\\\cline{2-3}\cline{2-3}
    &a&ax\\\cline{2-3}
    &x&\frac{x^2}{2}\\\cline{2-3}
    &x^2&\frac{x^3}{3}\\\cline{2-3}
    Pour $n\neq -1$ &x^n&\frac{x^{n+1}}{n+1}\\\cline{2-3}
    &\frac{1}{\sqrt{x}}&2\sqrt{x}\\\cline{2-3}
    &\sqrt{x}&\frac{2}{3}x\sqrt{x}\\\cline{2-3}
    &\frac{1}{x^2}&-\frac{1}{x}\\\cline{2-3}
    &\cos(x)&\sin(x)\\\cline{2-3}
    &\sin(x)&-\cos(x)\\\cline{2-3}
  \end{tabular}
\end{center}



En posant
\begin{equation*}
F(b)-F(a)=\left[F(x)\right]_a^b.
\end{equation*}



\begin{exem}
  $\int_1^6x^3\D{x}=\left[\frac{x^4}{4}\right]_1^6=\frac{6^4}{4}-\frac{1^4}{4}=323,75$
\end{exem}



\begin{exem}
  \begin{eqnarray*}
   \int_{-1}^23x^3-5x^2+x+2\D{x}&=&\left[\frac{3}{4}x^4-\frac{5}{3}x^3+\frac{1}{2}x^2+2x\right]_{-1}^2\\
&=&\left(\frac{3}{4}2^4-\frac{5}{3}2^3+\frac{1}{2}2^2+2\times2\right)-\left(\frac{3}{4}(-1)^4-\frac{5}{3}(-1)^3+\frac{1}{2}(-1)^2+2\times(-1)\right)\\
&=&\frac{15}{4}
 \end{eqnarray*}

\end{exem}

\begin{exem}
  $\int_1^4\frac{1}{x^2}\D{x}=\left[-\frac{1}{x}\right]_1^4=(-\frac{1}{4})-(-\frac{1}{1})=\frac{3}{4}$
\end{exem}

\begin{remarque}
  Le tableau ne donne qu'une primitive de $f$, les autres s'obtiennent
  en ajoutant une constante. \'Evidement, cela ne change pas la valeur
  de l'intégrale...

$\int_2^4x \D{x}=\left[\frac{x^2}{2}\right]_2^4=\left[\frac{x^2}{2}+112\right]_2^4=6$
\end{remarque}


\begin{bclogo}[arrondi = 0.1,logo=\bcdanger]{Attention!}
  \begin{itemize}
  \item Peut-on intégrer $x\longmapsto \frac{1}{x^2}$ entre $-1$ et $1$?
  \item Que vaut $\int_1^2\frac{1}{x}\D{x}$?
  \end{itemize}
\end{bclogo}

% \begin{bclogo}[arrondi = 0.1,logo=\bcdanger]{Attention!}
%   $\int_{-1}^1\frac{1}{x^2}\D{x}\neq -2$!!!


% La courbe qui représente $y=\frac{1}{x^2}$ n'est pas continue sur $[-1;1]$, on ne
% peut même pas écrire l'intégrale de $\frac{1}{x^2}$ entre $-1$ et $1$...


% \end{bclogo}

\section{Valeur moyenne}
\label{sec:valeur-moyenne}



Soit $f$ une fonction continue sur l'intervalle $[a;b]$ ($a<b$), la valeur
moyenne de $f$ sur l'intervalle $[a;b]$ est la quantité:

\begin{equation*}
  \frac{1}{b-a}\int_a^bf(t)\D{t}
\end{equation*}





\begin{center}
  \includegraphics[width=12cm]{le_canal_fig3}
\end{center}





\end{document}
