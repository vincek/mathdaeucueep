%%% Fichier mis à disposition selon la licence CC BY-NC-SA
%%% (https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) %%%

\documentclass[A4,sol]{cueep}

\renewcommand{\SFC}{\includegraphics[height=8mm]{../logo_lille1}\includegraphics[height=8mm]{../ccbyncsa}}
 
\usepackage{draftwatermark}
\SetWatermarkScale{1.7}
\SetWatermarkAngle{300}
\SetWatermarkLightness{0.95}
\SetWatermarkText{Math 9}
\nosolout=0

\titre{Au sujet des équations de droites}
\auteur{Sébastien Dumoulard}
\organisme{SFC}
\ladate{0.99}
\reference{Memo}
\motsclefs{Équations de droites, situation affine}

\usepackage{enumitem}
\usepackage{slashbox}

\newcommand{\operator}[3]{
\begin{tikzpicture}
\useasboundingbox (-2.5,-2.5) rectangle (7.5,2.5);
\tikzset{VertexStyle/.style = {shape= circle,shading= ball,ball color= yellow,minimum size = 50pt,draw,color=black}}
\tikzset{LabelStyle/.style = {draw,color=black,fill=white}}
\tikzset{EdgeStyle/.style = {->, bend left,thick,double= red,double distance = 1pt}}
\Vertex[L=#1]{a} 
\Vertex[L=#2,x=4,y=0]{b}

\Edge[label={#3}](a)(b)
\end{tikzpicture}}

\newcommand{\operatorr}[5]{
\begin{tikzpicture}
\useasboundingbox (-2.5,-2.5) rectangle (7.5,2.5);
\tikzset{VertexStyle/.style = {shape= circle,shading= ball,ball color= yellow,minimum size = 50pt,draw,color=black}}
\tikzset{LabelStyle/.style = {draw,color=black,fill=white}}
\tikzset{EdgeStyle/.style = {->, bend left,thick,double= red,double distance = 1pt}}
\Vertex[L=#1]{a} 
\Vertex[L=#2,x=4,y=0]{b}
\Vertex[L=#3,x=8,y=0]{c}


\Edge[label={#4}](a)(b)
\Edge[label={#5}](b)(c)
\end{tikzpicture}}


\newcommand{\operatorrr}[7]{
\begin{tikzpicture}\SetGraphUnit{4}
\useasboundingbox (-2.5,-2.5) rectangle (7.5,2.5);
\tikzset{VertexStyle/.style = {shape= circle,shading= ball,ball color= yellow,minimum size = 50pt,draw,color=black}}
\tikzset{LabelStyle/.style = {draw,color=black,fill=white}}
\tikzset{EdgeStyle/.style = {->, bend left,thick,double= red,double distance = 1pt}}
\Vertex[L=#1]{a} 
\Vertex[L=#2,x=4,y=0]{b}
\Vertex[L=#3,x=8,y=0]{c}
\Vertex[L=#4,x=12,y=0]{d}




\Edge[label={#5}](a)(b)
\Edge[label={#6}](b)(c)
\Edge[label={#7}](c)(d)
\end{tikzpicture}}




\begin{document}


\section{\'{E}quations de droites}
Une équation de droite est une équation qui caractérise, dans un repère, l'ensemble des points appartenant à cette droite.
\subsection{Des exemples simples}
\begin{description}
\item \underline{L'axe des abscisses} : ce qui caractérise les points appartenant à l'axe des abscisses, c'est le fait que leur ordonnée soit nulle. L'axe des abscisses est la droite d'équation $y=0$.
\item \underline{L'axe des ordonnées} : ce qui caractérise les points appartenant à l'axe des ordonnées, c'est le fait que leur abscisse soit nulle. L'axe des ordonnées est la droite d'équation $x=0$.
\item \underline{La diagonale principale du repère} : ce qui caractérise les points appartenant à cette diagonale, c'est le fait que leur abscisse soit égale à leur ordonnée. La diagonale principale du repère est la droite d'équation $y=x$.
\end{description}
\begin{figure}[htbp]
\centering
\includegraphics[scale=0.3]{images/tc06A.png}
\end{figure}
\subsection{Cas général}
\textbf{\underline{Propriété} :}\\
Toute droite non verticale admet une équation de la forme $y=ax+b$, où $a$ est un nombre appelé \textbf{coefficient directeur}, ou encore \textbf{pente} de la droite, et $b$ un nombre appelé \textbf{ordonnée à l'origine} de la droite.\\
Toute droite verticale admet une équation de la forme $x=a$.\\
\textbf{\underline{Exemple} : la droite d'équation $y=2x+1$}\\
Pour la tracer, il suffit de placer quelques points (au moins deux) dont les coordonnées $(x~;~y)$ vérifient l'équation, c'est à dire ici des points pour lesquels l'ordonnée ($y$) est égale à 2 fois l'abscisse augmenté de 1 ($2\times x+1$).\\
Si $x=0$, alors on obtient $y=2\times 0 +1 = 1$, donc le point $(0~;~1)$ est un point de la droite.\\
Si $x=3$, alors on obtient $y=2\times 3 +1 = 7$, donc le point $(3~;~7)$ est un point de la droite.\\
On peut aussi utiliser les coefficients $a$ et $b$ :\\
Ici, $b=1$, donc la droite passe par le point $(0~;~1)$.\\
De plus, la pente $a=2$ : d'un point de la droite, si on avance de 1 unité en abscisse, on monte de 2 unités en ordonnées.
\begin{figure}[htbp]
\centering
\includegraphics[scale=0.3]{images/tc06B.png}
\end{figure}

\pagebreak


Cette méthode est parfois plus adaptée, notamment en cas de coefficients fractionnaires.\\
Par exemple, si $a=2/3$, cela signifie que la pente de la droite vaut ``2 pour 3'' : on monte de 2 unités en ordonnée, pour une avancée de 3 unités en abscisse. \\
Cela permet de construire rapidement la droite d'équation $y=\dfrac{2}{3}x-4$.
\begin{figure}[htbp]
\centering
\includegraphics[scale=0.3]{images/tc06C.png}
\end{figure}

\subsection{Cas particuliers}
\begin{Itemize}
\item Lorsque $b=0$, alors la droite a pour équation $y=ax$ : c'est une \textbf{droite passant par l'origine du repère}. Elle est la représentation graphique d'une fonction \textbf{linéaire}, et modélise une situation de \textbf{proportionnalité} ($y$ est proportionnelle à $x$).
\item Lorsque $a=0$, alors la droite a pour équation $y=b$ : c'est une droite de pente nulle, autrement dit une \textbf{droite} \textbf{horizontale}. Elle est la représentation graphique d'une fonction \textbf{constante}.
\end{Itemize}

\subsection{Déterminer graphiquement l'équation d'une droite}
On détermine facilement le coefficient $b$ : c'est l'ordonnée du point de la droite d'abscisse nulle.\\
On détermine la pente $a$ par le rapport des accroissements de $y$ aux accroissements de $x$ : $a=\dfrac{\Delta_y}{\Delta_x}$.

\section{Caractérisation du modèle affine}
Une fonction affine de $x$ est une fonction $f$ dont l'expression est de la forme $f(x)=ax+b$. Les fonctions affines sont les fonctions représentées graphiquement par une droite (non verticale).\\
On démontre que les fonctions affines sont aussi les fonctions pour lesquelles \textbf{l'accroissement de la fonction est proportionnel à l'accroissement de la variable}. Le coefficient de proportionnalité associé est le nombre $a$.\\
Cette propriété permet de déterminer simplement l'expression d'une fonction affine $f$ dont on connait quelques valeurs :

\begin{tabular}{|c|c|}
\hline $x$ & $f(x)$ \\ 
\hline 0 & 5 \\ 
\hline 3 & 11 \\ 
\hline $-2$ & 1 \\ 
\hline 
\end{tabular} 

Le nombre $a$ vaut $\dfrac{11-5}{3-0}=\dfrac{1-11}{-2-3}=2$.\\
Par ailleurs $b=f(0)=5$.\\
Donc $f(x)=2x+5$.
\pagebreak
\section{Exercices}

\begin{exercice}
Dans le repère suivant, tracer les droites suivantes :
\begin{Itemize}
\item $(d_1)$ d'équation $y=-2x+1$
\item $(d_2)$ d'équation $y=\frac{x}{2}-2$
\item $(d_3)$ d'équation $y=1,5x+3$
\end{Itemize}
\begin{figure}[hbtp]
\includegraphics[scale=0.4]{images/repere_vierge.png}
\end{figure}

\begin{sol}
\begin{Itemize}
\item $(d_1)$ d'équation $y=-2x+1$
\item $(d_2)$ d'équation $y=\frac{x}{2}-2$
\item $(d_3)$ d'équation $y=1,5x+3$
\end{Itemize}
\begin{figure}[hbtp]
\includegraphics[scale=0.7]{images/repere_vierge2.png}
\end{figure}
\end{sol}

\end{exercice}


\begin{exercice}
Donner l'équation de chacune des cinq droites tracées dans le repère suivant.
\begin{figure}[hbtp]
\includegraphics[scale=0.35]{images/tc06_droites.png}
\end{figure}
\begin{sol}
\begin{Itemize}
\item $d_1:y=3x-2$
\item $d_2:y=-x+4$
\item $d_3:y=\dfrac{-2}{3}x$
\item $d_4:y=\dfrac{1}{4}x+1$
\item $d_5:y=-5$
\end{Itemize}
\end{sol}
\end{exercice}

\begin{exercice}
Déterminer l'expression de la fonction affine dont voici un tableau de valeurs.

\begin{tabular}{|c|c|}
\hline $x$ & $f(x)$ \\ 
\hline 3 & 5 \\ 
\hline 5 & 3 \\ 
\hline 
\end{tabular} 
\begin{sol}
Calcul de la pente $a=\dfrac{\Delta_y}{\Delta_x}=\dfrac{-2}{2}=-1$.\\
Donc $f(x)=-x+b$ où b reste à déterminer. \\
Cette formule donne $f(3)=-3+b$, mais le tableau donne aussi $f(3) = 5$.\\
On en déduit que $-3+b=5$, d'où $b=8$.\\
Conclusion : $f(x)=-x+8$.
\end{sol}
\end{exercice}
\pagebreak


\begin{exercice}
\begin{enumerate}
\item Tracer, dans le repère suivant, les droites $(d)$ et $(d')$ d'équations respectives $2x+y=3$ et $3x-5y=10$.
\item Déterminer par le calcul les coordonnées du point d'intersection des deux droites $(d)$ et $(d')$.
\end{enumerate}
\begin{figure}[hbtp]
\includegraphics[scale=0.35]{images/repere_vierge.png}
\end{figure}
\begin{sol}
Notons $I$ le point d'intersection de $(d)$ et $(d')$. Le calcul montre que $x_I = \dfrac{25}{13}$ et $y_I=\dfrac{-11}{13}$.
\end{sol}
\end{exercice}



\end{document}
